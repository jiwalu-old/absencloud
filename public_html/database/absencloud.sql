PGDMP     "    -                x         
   absencloud    11.5    11.5 8    h           0    0    ENCODING    ENCODING        SET client_encoding = 'UTF8';
                       false            i           0    0 
   STDSTRINGS 
   STDSTRINGS     (   SET standard_conforming_strings = 'on';
                       false            j           0    0 
   SEARCHPATH 
   SEARCHPATH     8   SELECT pg_catalog.set_config('search_path', '', false);
                       false            k           1262    18970 
   absencloud    DATABASE     �   CREATE DATABASE absencloud WITH TEMPLATE = template0 ENCODING = 'UTF8' LC_COLLATE = 'English_United States.1252' LC_CTYPE = 'English_United States.1252';
    DROP DATABASE absencloud;
             postgres    false                        2615    18974    app    SCHEMA        CREATE SCHEMA app;
    DROP SCHEMA app;
             postgres    false            �            1259    19094    attendance_spots_id_seq    SEQUENCE     �   CREATE SEQUENCE app.attendance_spots_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    MAXVALUE 10000
    CACHE 1;
 +   DROP SEQUENCE app.attendance_spots_id_seq;
       app       postgres    false    6            �            1259    19079    attendance_spots    TABLE     �  CREATE TABLE app.attendance_spots (
    id integer DEFAULT nextval('app.attendance_spots_id_seq'::regclass) NOT NULL,
    name character varying(150),
    code character varying(50),
    description text,
    address text,
    type character varying(30),
    area text,
    radius double precision,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);
 !   DROP TABLE app.attendance_spots;
       app         postgres    false    210    6            �            1259    19051    companies_id_seq    SEQUENCE     z   CREATE SEQUENCE app.companies_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    MAXVALUE 100000
    CACHE 1;
 $   DROP SEQUENCE app.companies_id_seq;
       app       postgres    false    6            �            1259    18995 	   companies    TABLE     �  CREATE TABLE app.companies (
    id integer DEFAULT nextval('app.companies_id_seq'::regclass) NOT NULL,
    name character varying(128),
    address text,
    phone character varying(24),
    fax character varying(24),
    email character varying(150),
    website character varying(150),
    business_field character varying(150),
    time_zone character varying(10),
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone,
    deleted_at timestamp(0) without time zone,
    code character varying(255),
    image character varying(150),
    province character varying(100),
    city character varying(120),
    zip_code character varying(5)
);
    DROP TABLE app.companies;
       app         postgres    false    203    6            �            1259    19055    menus_id_seq    SEQUENCE     t   CREATE SEQUENCE app.menus_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    MAXVALUE 1000
    CACHE 1;
     DROP SEQUENCE app.menus_id_seq;
       app       postgres    false    6            �            1259    19041    menus    TABLE     �   CREATE TABLE app.menus (
    id integer DEFAULT nextval('app.menus_id_seq'::regclass) NOT NULL,
    name character varying(128),
    title character varying(128),
    url character varying(255),
    parent integer,
    sort_order integer
);
    DROP TABLE app.menus;
       app         postgres    false    204    6            �            1259    19069    personnel_id_seq    SEQUENCE     z   CREATE SEQUENCE app.personnel_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    MAXVALUE 100000
    CACHE 1;
 $   DROP SEQUENCE app.personnel_id_seq;
       app       postgres    false    6            �            1259    19058 	   personnel    TABLE     �  CREATE TABLE app.personnel (
    id integer DEFAULT nextval('app.personnel_id_seq'::regclass) NOT NULL,
    name character varying(150),
    gender character varying(10),
    personnel_id integer,
    email character varying(150),
    phone double precision,
    start_work date,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone,
    deleted_at timestamp(0) without time zone,
    image character varying(150)
);
    DROP TABLE app.personnel;
       app         postgres    false    207    6            �            1259    19071    personnel_groups_id_seq    SEQUENCE     �   CREATE SEQUENCE app.personnel_groups_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    MAXVALUE 10000
    CACHE 1;
 +   DROP SEQUENCE app.personnel_groups_id_seq;
       app       postgres    false    6            �            1259    19064    personnel_groups    TABLE     &  CREATE TABLE app.personnel_groups (
    id integer DEFAULT nextval('app.personnel_groups_id_seq'::regclass) NOT NULL,
    name character varying(150),
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone,
    deleted_at timestamp(0) without time zone
);
 !   DROP TABLE app.personnel_groups;
       app         postgres    false    208    6            �            1259    19030    sessions    TABLE     �   CREATE TABLE app.sessions (
    id character varying(128) NOT NULL,
    ip_address character varying(45) NOT NULL,
    "timestamp" integer,
    data text
);
    DROP TABLE app.sessions;
       app         postgres    false    6            �            1259    19003    user_groups    TABLE       CREATE TABLE app.user_groups (
    id integer NOT NULL,
    name character varying(128),
    title character varying(128),
    privileges json,
    created_at timestamp(6) without time zone DEFAULT now(),
    updated_at timestamp(6) without time zone,
    company_id integer
);
    DROP TABLE app.user_groups;
       app         postgres    false    6            �            1259    19049    users_id_seq    SEQUENCE     v   CREATE SEQUENCE app.users_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    MAXVALUE 100000
    CACHE 1;
     DROP SEQUENCE app.users_id_seq;
       app       postgres    false    6            �            1259    19010    users    TABLE     V  CREATE TABLE app.users (
    id integer DEFAULT nextval('app.users_id_seq'::regclass) NOT NULL,
    company_id integer NOT NULL,
    group_id integer,
    username character varying(128) NOT NULL,
    password character varying(128) NOT NULL,
    email character varying(128),
    name character varying(128),
    image character varying(256),
    status character varying,
    last_login timestamp(6) without time zone,
    created_at timestamp(6) without time zone,
    updated_at timestamp(6) without time zone,
    deleted_at timestamp(6) without time zone,
    token character varying(128)
);
    DROP TABLE app.users;
       app         postgres    false    202    6            �            1259    19103    work_pattern_detail    TABLE     �   CREATE TABLE app.work_pattern_detail (
    id integer,
    name character varying(150),
    clock_in time(0) without time zone,
    clock_out time(0) without time zone,
    created_at timestamp(0) without time zone
);
 $   DROP TABLE app.work_pattern_detail;
       app         postgres    false    6            �            1259    19113    work_pattern_detail_id_seq    SEQUENCE     �   CREATE SEQUENCE app.work_pattern_detail_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    MAXVALUE 10000
    CACHE 1;
 .   DROP SEQUENCE app.work_pattern_detail_id_seq;
       app       postgres    false    6            �            1259    19109    work_patterns_id_seq    SEQUENCE     ~   CREATE SEQUENCE app.work_patterns_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    MAXVALUE 100000
    CACHE 1;
 (   DROP SEQUENCE app.work_patterns_id_seq;
       app       postgres    false    6            �            1259    19098    work_patterns    TABLE     K  CREATE TABLE app.work_patterns (
    id integer DEFAULT nextval('app.work_patterns_id_seq'::regclass) NOT NULL,
    name character varying(150),
    day_cycle integer,
    tolerance real,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone,
    deleted_at timestamp(0) without time zone
);
    DROP TABLE app.work_patterns;
       app         postgres    false    213    6            �            1259    19123    work_time_id_seq    SEQUENCE     y   CREATE SEQUENCE app.work_time_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    MAXVALUE 10000
    CACHE 1;
 $   DROP SEQUENCE app.work_time_id_seq;
       app       postgres    false    6            �            1259    19117 	   work_time    TABLE     m  CREATE TABLE app.work_time (
    id integer DEFAULT nextval('app.work_time_id_seq'::regclass) NOT NULL,
    name character varying(150),
    personnel_id integer,
    valid date,
    day integer,
    work_pattern integer,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone,
    deleted_at timestamp(0) without time zone
);
    DROP TABLE app.work_time;
       app         postgres    false    216    6            ^          0    19079    attendance_spots 
   TABLE DATA               y   COPY app.attendance_spots (id, name, code, description, address, type, area, radius, created_at, updated_at) FROM stdin;
    app       postgres    false    209   CD       R          0    18995 	   companies 
   TABLE DATA               �   COPY app.companies (id, name, address, phone, fax, email, website, business_field, time_zone, created_at, updated_at, deleted_at, code, image, province, city, zip_code) FROM stdin;
    app       postgres    false    197   �D       V          0    19041    menus 
   TABLE DATA               F   COPY app.menus (id, name, title, url, parent, sort_order) FROM stdin;
    app       postgres    false    201   �E       Z          0    19058 	   personnel 
   TABLE DATA               �   COPY app.personnel (id, name, gender, personnel_id, email, phone, start_work, created_at, updated_at, deleted_at, image) FROM stdin;
    app       postgres    false    205   �F       [          0    19064    personnel_groups 
   TABLE DATA               U   COPY app.personnel_groups (id, name, created_at, updated_at, deleted_at) FROM stdin;
    app       postgres    false    206   eG       U          0    19030    sessions 
   TABLE DATA               B   COPY app.sessions (id, ip_address, "timestamp", data) FROM stdin;
    app       postgres    false    200   �G       S          0    19003    user_groups 
   TABLE DATA               c   COPY app.user_groups (id, name, title, privileges, created_at, updated_at, company_id) FROM stdin;
    app       postgres    false    198   �H       T          0    19010    users 
   TABLE DATA               �   COPY app.users (id, company_id, group_id, username, password, email, name, image, status, last_login, created_at, updated_at, deleted_at, token) FROM stdin;
    app       postgres    false    199   �I       a          0    19103    work_pattern_detail 
   TABLE DATA               U   COPY app.work_pattern_detail (id, name, clock_in, clock_out, created_at) FROM stdin;
    app       postgres    false    212   �J       `          0    19098    work_patterns 
   TABLE DATA               h   COPY app.work_patterns (id, name, day_cycle, tolerance, created_at, updated_at, deleted_at) FROM stdin;
    app       postgres    false    211   K       d          0    19117 	   work_time 
   TABLE DATA               v   COPY app.work_time (id, name, personnel_id, valid, day, work_pattern, created_at, updated_at, deleted_at) FROM stdin;
    app       postgres    false    215   KK       l           0    0    attendance_spots_id_seq    SEQUENCE SET     B   SELECT pg_catalog.setval('app.attendance_spots_id_seq', 2, true);
            app       postgres    false    210            m           0    0    companies_id_seq    SEQUENCE SET     ;   SELECT pg_catalog.setval('app.companies_id_seq', 5, true);
            app       postgres    false    203            n           0    0    menus_id_seq    SEQUENCE SET     8   SELECT pg_catalog.setval('app.menus_id_seq', 11, true);
            app       postgres    false    204            o           0    0    personnel_groups_id_seq    SEQUENCE SET     B   SELECT pg_catalog.setval('app.personnel_groups_id_seq', 2, true);
            app       postgres    false    208            p           0    0    personnel_id_seq    SEQUENCE SET     ;   SELECT pg_catalog.setval('app.personnel_id_seq', 1, true);
            app       postgres    false    207            q           0    0    users_id_seq    SEQUENCE SET     7   SELECT pg_catalog.setval('app.users_id_seq', 4, true);
            app       postgres    false    202            r           0    0    work_pattern_detail_id_seq    SEQUENCE SET     F   SELECT pg_catalog.setval('app.work_pattern_detail_id_seq', 1, false);
            app       postgres    false    214            s           0    0    work_patterns_id_seq    SEQUENCE SET     @   SELECT pg_catalog.setval('app.work_patterns_id_seq', 13, true);
            app       postgres    false    213            t           0    0    work_time_id_seq    SEQUENCE SET     ;   SELECT pg_catalog.setval('app.work_time_id_seq', 1, true);
            app       postgres    false    216            �
           2606    19086 &   attendance_spots attendance_spots_pkey 
   CONSTRAINT     a   ALTER TABLE ONLY app.attendance_spots
    ADD CONSTRAINT attendance_spots_pkey PRIMARY KEY (id);
 M   ALTER TABLE ONLY app.attendance_spots DROP CONSTRAINT attendance_spots_pkey;
       app         postgres    false    209            �
           2606    19002    companies companies_pk 
   CONSTRAINT     Q   ALTER TABLE ONLY app.companies
    ADD CONSTRAINT companies_pk PRIMARY KEY (id);
 =   ALTER TABLE ONLY app.companies DROP CONSTRAINT companies_pk;
       app         postgres    false    197            �
           2606    19048    menus menus_pkey 
   CONSTRAINT     K   ALTER TABLE ONLY app.menus
    ADD CONSTRAINT menus_pkey PRIMARY KEY (id);
 7   ALTER TABLE ONLY app.menus DROP CONSTRAINT menus_pkey;
       app         postgres    false    201            �
           2606    19068 %   personnel_groups personnel_group_pkey 
   CONSTRAINT     `   ALTER TABLE ONLY app.personnel_groups
    ADD CONSTRAINT personnel_group_pkey PRIMARY KEY (id);
 L   ALTER TABLE ONLY app.personnel_groups DROP CONSTRAINT personnel_group_pkey;
       app         postgres    false    206            �
           2606    19062    personnel personnel_pkey 
   CONSTRAINT     S   ALTER TABLE ONLY app.personnel
    ADD CONSTRAINT personnel_pkey PRIMARY KEY (id);
 ?   ALTER TABLE ONLY app.personnel DROP CONSTRAINT personnel_pkey;
       app         postgres    false    205            �
           2606    19037    sessions sessions_pk 
   CONSTRAINT     [   ALTER TABLE ONLY app.sessions
    ADD CONSTRAINT sessions_pk PRIMARY KEY (id, ip_address);
 ;   ALTER TABLE ONLY app.sessions DROP CONSTRAINT sessions_pk;
       app         postgres    false    200    200            �
           2606    19017    user_groups user_groups_pk 
   CONSTRAINT     U   ALTER TABLE ONLY app.user_groups
    ADD CONSTRAINT user_groups_pk PRIMARY KEY (id);
 A   ALTER TABLE ONLY app.user_groups DROP CONSTRAINT user_groups_pk;
       app         postgres    false    198            �
           2606    19019    users users_pk 
   CONSTRAINT     I   ALTER TABLE ONLY app.users
    ADD CONSTRAINT users_pk PRIMARY KEY (id);
 5   ALTER TABLE ONLY app.users DROP CONSTRAINT users_pk;
       app         postgres    false    199            �
           2606    19102    work_patterns work_pattern_pkey 
   CONSTRAINT     Z   ALTER TABLE ONLY app.work_patterns
    ADD CONSTRAINT work_pattern_pkey PRIMARY KEY (id);
 F   ALTER TABLE ONLY app.work_patterns DROP CONSTRAINT work_pattern_pkey;
       app         postgres    false    211            �
           2606    19020    users users_fk    FK CONSTRAINT     �   ALTER TABLE ONLY app.users
    ADD CONSTRAINT users_fk FOREIGN KEY (company_id) REFERENCES app.companies(id) ON DELETE CASCADE;
 5   ALTER TABLE ONLY app.users DROP CONSTRAINT users_fk;
       app       postgres    false    2758    199    197            �
           2606    19025    users users_fk_1    FK CONSTRAINT     p   ALTER TABLE ONLY app.users
    ADD CONSTRAINT users_fk_1 FOREIGN KEY (group_id) REFERENCES app.user_groups(id);
 7   ALTER TABLE ONLY app.users DROP CONSTRAINT users_fk_1;
       app       postgres    false    198    199    2760            ^   �   x�=�M
�0�u<E`�{/�C����E�5U�J��k�v�3�b�a�e�h�FV�l�r�u9���K?�C�/���-�V��u�����M#�*a$��̥	�2�2l�ݕ�4�5 �Ñ���tE���Q��
ܹ��s��6��� -�8B� G�qR�$I>e�:�      R   �   x�U��n�0���S��*I�4�M\���q��ŅR�vJ����
TH�%��?�0�j�9{��{h�K��g�k�k�+�P~��#.��pA�.�U���I��\\��j���=�l�mW� ��#�&$d�*�BɌ�i��Jn�)�N��>UPSA��I�<��I&r�ޕW�.?z,�������Q�������)���t00��m���[_      V   �   x�m��n� D�s��R�u���e�HY���E7�,��jSU��~ĉ�1>p��	V��h��U-�D9b��8��7s��g�3?v��$��'81Բ���j����� ������?#�S�9����u�h���`Z+�_�������웳�:�}�OxO�m\�Kb�YY�r�9+�6�W�����6�I˩�u��(�u���C�Q�k�������6��y�$.Osy��yI�"����      Z   v   x�UȻ�0 ��<E����9W� �\�@F���~�tO�������^@,t�z߷���
�\�,���8`�G�Qf��'9NYF�P9�e4}�$\*YH���I���|��;�W�"�      [   G   x�3���,��)�4202�50�50T04�26�24��".C���ĜR�*�L���L��L��Yp��qqq ��      U   .  x�M��r�0���a:$gz�ZapF2N� �
	�`G���7����s�y�y-��뉪��,H�H����*��h6C#D�."9�Q��&�z�4���Y�oOY��e���q���ƫxcG���b}�p)3�;�҂6�	����$��Ҝ6;��Y���9[��q+�y8)n혊�*�*#�{���6���.&o �K�I��=|7l6V����+%+��]�>p��� .9���z���%���h��2� �(�O�Z1��72^�4��.۰����߷�9;`���e����)u�g��ɲ�o�މz      S   }   x�ʱ�0��9y��3�|Nho<0T��C�Tʄxw���~	�c]�p�]��>�=|i}n�+�N$&�b�f2�4���Kx
o�1���t�a�N��d-E����{�K8�$���,���c�
�!�      T   .  x�m��n�@Eד�`�6�x<�UShY@EE��$@��I[���$K����k3��.�%i�w���\�ob����\�Ɔ���@O��{��*�Sɼ4ifx�P%8�\��
���z9#Η�GFe4�"4��,UL�� aQ[��Rp��O��N):��y�I��OY½���,�F*GA���w�[ﻧ��|�[��F�c�����m��CtF�b^�����`펛����]�$���8D�N�g�h���Vn����׹��u>=� {y)k �BYq�8�� �� v      a   -   x�34�,�/���KWHI��4��20 "NCs(#ƏːJjb���� h%      `   9   x�34���,O�)U�K�M�4�46�4202�50�50S0��24�25��"�=... <<
�      d   C   x�3�L�I�-��(MI�4�4202�50�54��44�������LPČ��̀�3Ə+F��� ���     