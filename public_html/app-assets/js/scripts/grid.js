Object.prototype.hasOwnProperty = function (property) {
  return this[property] !== undefined;
};

$.extend($.fn.dataTable.defaults, {
  // processing: true,
  serverSide: true,
  lengthMenu: [
    [10, 20, 50, 100, 150],
    [10, 20, 50, 100, 150]
  ],
  autoWidth: false,
  pageLength: 20,
  //"bStateSave": true,
  // dom: '<"datatable-header"Bl>r<"datatable-body"t><"datatable-footer"ip>',
  // dom: '<"dt-panelmenu clearfix"lrB>t<"dt-panelfooter clearfix"ip>',
  dom: "<'table-responsive't><'row'<'col-6'li><'col-6'p>>",
  language: {
    search: '<span>Filter:</span> _INPUT_',
    lengthMenu: '<span>Show:</span> _MENU_',
    emptyTable: 'No data available',
    zeroRecords: 'No matching records found',
    paginate: { 'first': '&laquo;', 'last': '&raquo;', 'next': '&rsaquo;', 'previous': '&lsaquo;' }
  },
  pagingType: "full_numbers",
  // buttons: ['copy', 'excel', 'csv', 'pdf', 'print'],
  buttons: [
    {
      extend: 'collection',
      text: '<i class="fa fa-navicon"></i>',
      className: 'btn btn-icon btn-default',
      buttons: [
        {
          extend: 'print',
          text: '<i class="fa fa-print position-left"></i> Print'
        },
        {
          extend: 'excel',
          text: '<i class="fa fa-file-excel-o position-left"></i> Excel'
        },
        {
          extend: 'csv',
          text: '<i class="fa fa-file-excel-o position-left"></i> CSV'
        },
        {
          extend: 'pdf',
          text: '<i class="fa fa-file-pdf-o position-left"></i> PDF'
        },
        {
          extend: 'copy',
          text: '<i class="fa fa-copy position-left"></i> Copy'
        }
      ]
    }
  ],
  drawCallback: function () {
    // $('select').select2({
    //     minimumResultsForSearch: Infinity
    // });
    // $('select.with_search').select2();
    // $('.dataTables_length select').select2({
    //     minimumResultsForSearch: Infinity,
    //     width: 'auto'
    // });
    $('.date').pickadate({
      format: "yyyy-mm-dd",
      selectYears: true,
      selectMonths: true
    });
    $(this).find('tbody tr').slice(-3).find('.dropdown, .btn-group').addClass('dropup');
    $('[data-toggle=tooltip]').tooltip();
  },
  preDrawCallback: function () {
    $(this).find('tbody tr').slice(-3).find('.dropdown, .btn-group').removeClass('dropup');
  }
});

function confirm_bulk_action(confirm_title, confirm_message, action, success_title, success_message) {
  var modal = '<div class="modal fade slide-up disable-scroll" id="modalConfirmBulkAction" tabindex="-1" role="dialog">'
    + '<div class="modal-dialog modal-sm modal-dialog-centered">'
    + '<div class="modal-content-wrapper">'
    + '<div class="modal-content">'
    + '<div class="modal-header"><h5 class="modal-title">Confirmation</h5>'
    + '<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button></div>'
    + '<div class="modal-body text-center m-t-20">'
    + '<p class="no-margin p-b-10">' + confirm_title + '</p>'
    + '</div>'
    + '<div class="modal-footer justify-content-center">'
    + '<button type="button" class="btn btn-danger mr-2" data-dismiss="modal" onclick="ajax_bulk_action(\'' + action + '\', \'' + success_title + '\', \'' + success_message + '\');">Confirm</button>'
    + '<button type="button" class="btn btn-outline-warning" data-dismiss="modal">Cancel</button>'
    + '</div>'
    + '</div>'
    + '</div>'
    + '</div>'
    + '</div>';
  $('body').append(modal);
  $('#modalConfirmBulkAction').modal('show');

  $('#modalConfirmBulkAction').on('hidden.bs.modal', function (e) {
    $('#modalConfirmBulkAction').remove();
  });
}

function ajax_bulk_action(action, success_title, success_message) {
  var data = [];
  $('input[name="data[]"]:checked').each(function (i) {
    data[i] = $(this).val();
  });
  $.ajax({
    url: module.url + '/' + action,
    data: { data: data, absen_token = module.hash },
    method: 'post',
    success: function (response) {
      if (response === "success") {
        toastr.success(success_message, 'Success', {"closeButton": true});
        module.table.ajax.reload(null, false);
        $('#bulk-action-apply').attr('disabled', true);
      }
    }
  });
}

$(document).ready(function () {
  // $('select').select2({
  //     minimumResultsForSearch: Infinity
  // });
  // $('select.with_search').select2();
  // $('.dataTables_length select').select2({
  //     minimumResultsForSearch: Infinity,
  //     width: 'auto'
  // });
  $('.date').pickadate({
    format: "yyyy-mm-dd",
    selectYears: true,
    selectMonths: true
  });
  $('body').on('click', '#checkAll', function () {
    $('input:checkbox').not(this).prop('checked', this.checked);
  });
  $('body').on('change', '#checkAll, input[name="data[]"]', function () {
    if ($('input[name="data[]"]:checked').length > 0) {
      $('#bulk-action-apply').attr('disabled', false);
    } else {
      $('#bulk-action-apply').attr('disabled', true);
    }
  });
  $('body').on('click', '.btn-add, .btn-edit', function(e){
    e.preventDefault();
    $.get($(this).attr('href'), function (response) {
      $('#formModal .modal-content').html(response);
      $('#formModal').modal('show');
    });
  });

  if (module.hasOwnProperty('table')) {
    module.table.columns().every(function () {
      var that = this;
      var search = $.fn.dataTable.util.throttle(
        function (val) {
          that.search(val).draw();
        }, 1000
      );
      $('input', this.footer()).on('keyup', function (e) {
        if (e.keyCode == 13) {
          search(this.value);
        }
      });
      $('input', this.footer()).on('change', function () {
        that.search(this.value).draw();
      });
      $('select', this.footer()).on('change', function () {
        that.search(this.value).draw();
      });
    });
  }

  var alert_success = 'Success!';
  var alert_delete_title = 'Are you sure you want to delete the selected item?';
  var alert_delete_text = 'Data will be deleted permanently and cannot be restored.';
  var alert_delete_success = 'Item successfully deleted.';
  var alert_remove_title = 'Are you sure?';
  var alert_remove_text = 'Data will be moved to trash.';
  var alert_remove_success = '';
  var alert_restore_title = '';
  var alert_restore_text = '';
  var alert_restore_success = '';

  // Bulk action
  $('body').on('click', '#bulk-action-apply', function (e) {
    e.preventDefault();
    var action = $('#bulk-action-select').val();
    if (action == 'trash') {
      confirm_bulk_action(alert_remove_title, alert_remove_text, action, alert_success, alert_remove_success);
    } else if (action == 'delete') {
      confirm_bulk_action(alert_delete_title, alert_delete_text, action, alert_success, alert_delete_success);
    } else if (action == 'restore') {
      ajax_bulk_action(action, alert_success, alert_restore_success);
    }
  });

  // ajax button
  $('body').on('click', '.btn-ajax', function (e) {
    e.preventDefault();
    var url = $(this).attr('href');
    $.ajax({
      url: url,
      method: 'get',
      success: function (response) {
        module.table.ajax.reload(null, false);
      }
    });
  });

  // delete button
  $('body').on('click', '.btn-delete', function (e) {
    e.preventDefault();
    var url = $(this).attr('href');
    var modal = '<div class="modal fade slide-up disable-scroll" id="modalConfirmDelete" tabindex="-1" role="dialog">'
      + '<div class="modal-dialog modal-sm modal-dialog-centered">'
      + '<div class="modal-content-wrapper">'
      + '<div class="modal-content">'
      + '<div class="modal-header"><h5 class="modal-title">Confirmation</h5>'
      + '<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button></div>'
      + '<div class="modal-body text-center m-t-20">'
      + '<p class="no-margin p-b-10">' + alert_delete_title + '</p>'
      + '</div>'
      + '<div class="modal-footer justify-content-center">'
      + '<button type="button" class="btn btn-danger btn-confirm mr-2" data-dismiss="modal" data-url="' + url + '">Confirm</button>'
      + '<button type="button" class="btn btn-outline-warning" data-dismiss="modal">Cancel</button>'
      + '</div>'
      + '</div>'
      + '</div>'
      + '</div>'
      + '</div>';
    $('body').append(modal);
    $('#modalConfirmDelete').modal('show');

    $('#modalConfirmDelete').on('hidden.bs.modal', function (e) {
      $('#modalConfirmDelete').remove();
    });
  });

  $('body').on('click', '#modalConfirmDelete .btn-confirm', function (e) {
    var url = $(this).attr('data-url');
    $.get(url, function (response) {
      if (response === "success") {
        toastr.success(alert_delete_success, 'Success', {"closeButton": true});
        module.table.ajax.reload(null, false);
      }
    });
  });

  // Restore button
  $('body').on('click', '.btn-restore', function (e) {
    e.preventDefault();
    var url = $(this).attr('href');
    $.ajax({
      url: url,
      method: 'post',
      success: function (response) {
        if (response == "success") {
          swal(alert_success, alert_restore_success, 'success');
        }
        module.table.ajax.reload(null, false);
      }
    });
  });

  // delete permanent
  $('body').on('click', '.btn-delete-permanent', function (e) {
    e.preventDefault();
    var url = $(this).attr('href');
    swal({
      title: alert_delete_title,
      text: alert_delete_text,
      type: "warning",
      showCancelButton: true,
      confirmButtonColor: "#f44336",
      confirmButtonText: "Confirm"
    }, function (isConfirm) {
      if (isConfirm) {
        $.ajax({
          url: url,
          method: 'post',
          success: function (response) {
            if (response == "success") {
              swal(alert_success, alert_delete_success, 'success');
            }
            module.table.ajax.reload(null, false);
          }
        });
      }
    });
  });

  // empty trash
  $('body').on('click', '#btn_empty_trash', function (e) {
    e.preventDefault();
    swal({
      title: alert_delete_title,
      text: alert_delete_text,
      type: "warning",
      showCancelButton: true,
      confirmButtonColor: "#f44336",
      confirmButtonText: "Confirm"
    }, function (isConfirm) {
      if (isConfirm) {
        $.ajax({
          url: module_url + '/empty_trash',
          method: 'post',
          success: function (response) {
            if (response == "success") {
              swal(alert_success, alert_delete_success, 'success');
            }
            module.table.ajax.reload(null, false);
          }
        });
      }
    });
  });
});
