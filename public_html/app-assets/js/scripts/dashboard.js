var link = base_url+'dashboard/';
var valId ='';
 
// Pusher.logToConsole = true;

// var pusher = new Pusher('4c14965b64bb3e5ebdc9', {
//   cluster: 'ap1',
//   forceTLS: true
// });

// var channel = pusher.subscribe('absen-channel');
// channel.bind('my-event', function(data) {    
//   if(typeof data['value']['url'] !== 'undefined'){
//     window.open(data['value']['url']);
//   }else{
//     console.log("URL does'n exist");
//   }
//   if (typeof data['value']['image'] !== 'undefined') {
//     $('#avatarprofile').attr('src',  base_url+'app-assets/images/user/'+data['value']['image']);
//   }
   
//   // notif();
// });
 
$(function() {    
  // notif();
  // setInterval(function(){ 
	// 	notif();
  // }, 30000);
// ==================================================================================  
  load = '<div class="row d-flex justify-content-center" style="padding-top: 120px;padding-bottom: 120px;">'+
			'<div class="spinner-grow text-primary" role="status">'+
				'<span class="sr-only">Loading...</span>'+
			'</div>'+
			'<div class="spinner-grow text-secondary" role="status">'+
				'<span class="sr-only">Loading...</span>'+
			'</div>'+
			'<div class="spinner-grow text-success" role="status">'+
				'<span class="sr-only">Loading...</span>'+
			'</div>'+
			'<div class="spinner-grow text-danger" role="status">'+
				'<span class="sr-only">Loading...</span>'+
			'</div>'+
			'<div class="spinner-grow text-warning" role="status">'+
				'<span class="sr-only">Loading...</span>'+
			'</div>'+
		'</div>';
  $("#contentPie").html(load); 
  $("#contentBar").html(load); 

  notfound = '<div id="notfound">'+
                '<div class="notfound">'+
                  '<div class="notfound-404">'+
                    '<h1>Oops!</h1>'+
                   '<h2>Data Not Found</h2>'+
                  '</div>'+ 
                '</div>'+
              '</div>'; 

});

  

function notif(){
   

  var unread = 0;
  var content ='';
  $.ajax({
    type : 'GET',
    url : link+"getNotif",
    dataType : 'json',
    success : function(data){ 
      $.each(data, function(i,item){ 
        if(item.is_read=='t'){
          back = '';
          weight = 'normal';
          icon = '-open';
        }else{
          unread++;
          weight = 'bold';
          back = 'aliceblue';
          icon ='';
        }
        content += '<a class="d-flex justify-content-between" href="javascript:void(0);" onclick="dashreadNotif('+item.id+')"> '+
                '<div class="media d-flex align-items-start" style="background-color:'+back+';">'+
                '<div class="media-left"><i class="fa fa-envelope'+icon+' font-medium-5 primary"></i></div>'+
                  '<div class="media-body">'+
                    '<h6 class="primary media-heading" style="font-weight:'+weight+';">'+item.title+'</h6>'+
                      '<small class="notification-text">  </small>'+
                  '</div>'+
                  '<small><time class="media-meta" datetime="2015-06-11T18:29:20+08:00">'+ moment(item.notiftime).fromNow() +' </time></small></div><a>';
        
      });
      if(unread>0){
        $("#totalnotif").html(unread);
      }
      
      $("#content-notif").html(content);
    }
  }); 
}

function dashreadNotif(valId){ 
  $.ajax({
    type : 'get',
    url : link+"updateNotif",
    data : ({id:valId}),
    dataType :'json',
    success : function(data){
      notif();       
    }
  });
}
 