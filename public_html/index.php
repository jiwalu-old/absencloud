<?php
/**
* Jiwalu Framework
* A framework for PHP development
*
* @package     Jiwalu Framework
* @author      Jiwalu Studio
* @copyright   Copyright (c) 2019, Jiwalu Studio (https://www.jiwalu.id)
*/

define('ENVIRONMENT', isset($_SERVER['CI_ENV']) ? $_SERVER['CI_ENV'] : 'development');

date_default_timezone_set('Asia/Jakarta');
 /*
 * -------------------------------------------------------------------
 *  PATH DEFINITIONS
 * -------------------------------------------------------------------
 */
$app_path = '../app';
$framework_path = '../system';

/*
 * -------------------------------------------------------------------
 *  Bootstrap
 * -------------------------------------------------------------------
 */
require_once '../framework.php';
