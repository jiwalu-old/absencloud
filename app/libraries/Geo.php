<?php (defined('BASEPATH')) OR exit('No direct script access allowed');

/**
* Jiwalu Framework
* A framework for PHP development
*
* @package     Jiwalu Framework
* @author      Jiwalu Studio
* @copyright   Copyright (c) 2019, Jiwalu Studio (https://www.jiwalu.id)
*/

require_once APPPATH."/third_party/PHPGeo/autoload.php";
use Location\Coordinate;
use Location\Distance\Vincenty;
use Location\Formatter\Coordinate\DecimalDegrees;
use Location\Line;
use Location\Polygon;
use Location\Polyline;
use Location\Formatter\Polygon\GeoJSON;

class Geo {
    public function is_geofence($params = [])
    {
        if ($params['type'] == 'polygon') {
            $geofence = new Polygon();
            foreach ($params['coords'] as $area_point) {
                $geofence->addPoint(new Coordinate($area_point[0], $area_point[1]));
            }

            $point = new Coordinate($params['latitude'], $params['longitude']);

            return $geofence->contains($point);

        } else if ($params['type'] == 'circle') {
            // $distance = round(( 6371 * acos(cos(deg2rad($coordinates->latitude)) * cos(deg2rad($latitude)) * cos(deg2rad($longitude) - deg2rad($coordinates->longitude)) + sin(deg2rad($coordinates->latitude)) * sin(deg2rad($latitude)))), 2);
            $distance = round((6371 * acos( cos( deg2rad($coordinates->latitude ) )
                                    * cos( deg2rad($latitude) )
                                    * cos( deg2rad($longitude) - deg2rad($coordinates->longitude) )
                                    + sin( deg2rad($coordinates->latitude) )
                                    * sin( deg2rad($latitude) )
                        )),2);
            return $distance <= $radius;
        }
    }

    public function get_distance($start_latitude, $start_longitude, $stop_latitude, $stop_longitude)
    {
        $coordinate1 = new Coordinate($start_latitude, $start_longitude);
        $coordinate2 = new Coordinate($stop_latitude, $stop_longitude);

        $calculator = new Vincenty();

        return $calculator->getDistance($coordinate1, $coordinate2);
    }
}
