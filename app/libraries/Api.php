<?php (defined('BASEPATH')) OR exit('No direct script access allowed');

/**
* Jiwalu Framework
* A framework for PHP development
*
* @package     Jiwalu Framework
* @author      Jiwalu Studio
* @copyright   Copyright (c) 2019, Jiwalu Studio (https://www.jiwalu.id)
*/

class Api {
    public function send_request($endpoint, $type, $data)
    {
        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_PORT => ABSEN_API_PORT,
            CURLOPT_URL => ABSEN_API_URL.$endpoint,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => $type,
            CURLOPT_POSTFIELDS => $data,
            CURLOPT_HTTPHEADER => array(
                "Authorization: ".ABSEN_API_AUTH,
                "Content-Type: application/json",
                "cache-control: no-cache"
            ),
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {
            return false;
        } else {
            return $response;
        }
    }

    /*
    Function : Get Device
    Return : Device Data
    */
    public function get_device($params)
    {
        $params = http_build_query($params);
        $request = $this->send_request('devices?'.$params, 'GET', json_encode($params));
        if ($request) {
            $result = json_decode($request);
            return $result;
        } else {
            return false;
        }
    }

    /*
    Function : Create Device
    Return : Device ID
    */
    public function create_device($data)
    {
        $request = $this->send_request('devices', 'POST', json_encode($data));
        if ($request) {
            $result = json_decode($request);
            return $result->id;
        } else {
            return false;
        }
    }

    /*
    Function : Update Device
    Return : Device ID
    */
    public function update_device($id, $data)
    {
        $request = $this->send_request('devices/'.$id, 'PUT', json_encode($data));
        if ($request) {
            $result = json_decode($request);
            return $result->id;
        } else {
            return false;
        }
    }

    /*
    Function : Update Device Accumulators
    Return : NULL
    */
    public function update_device_accumulator($id, $data)
    {
        $request = $this->send_request('devices/'.$id.'/accumulators', 'PUT', json_encode($data));
        if ($request) {
            $result = json_decode($request);
            return $result;
        } else {
            return false;
        }
    }

    /*
    Function : Delete Device
    Return : None
    */
    public function delete_device($id)
    {
        $request = $this->send_request('devices/'.$id, 'DELETE', json_encode($id));
        return $request;
    }

    /*
    Function : Get Driver
    Return : Driver Data
    */
    public function get_driver($params)
    {
        $params = http_build_query($params);
        $request = $this->send_request('drivers?'.$params, 'GET', json_encode($params));
        if ($request) {
            $result = json_decode($request);
            return $result;
        } else {
            return false;
        }
    }

    /*
    Function : Create Driver
    Return : Driver ID
    */
    public function create_driver($data)
    {
        $request = $this->send_request('drivers', 'POST', json_encode($data));
        if ($request) {
            $result = json_decode($request);
            return $result->id;
        } else {
            return false;
        }
    }

    /*
    Function : Update Driver
    Return : Driver ID
    */
    public function update_driver($id, $data)
    {
        $request = $this->send_request('drivers/'.$id, 'PUT', json_encode($data));
        if ($request) {
            $result = json_decode($request);
            return $result->id;
        } else {
            return false;
        }
    }

    /*
    Function : Delete Driver
    Return : None
    */
    public function delete_driver($id)
    {
        $request = $this->send_request('drivers/'.$id, 'DELETE', json_encode($id));
        return $request;
    }

    /*
    Function : Get Geofence
    Return : Geofence Data
    */
    public function get_geofence($params)
    {
        $params = http_build_query($params);
        $request = $this->send_request('geofences?'.$params, 'GET', json_encode($params));
        if ($request) {
            $result = json_decode($request);
            return $result;
        } else {
            return false;
        }
    }

    /*
    Function : Create Geofence
    Return : Geofence ID
    */
    public function create_geofence($data)
    {
        $request = $this->send_request('geofences', 'POST', json_encode($data));
        if ($request) {
            $result = json_decode($request);
            return $result->id;
        } else {
            return false;
        }
    }

    /*
    Function : Update Geofence
    Return : Geofence ID
    */
    public function update_geofence($id, $data)
    {
        $request = $this->send_request('geofences/'.$id, 'PUT', json_encode($data));
        if ($request) {
            $result = json_decode($request);
            return $result->id;
        } else {
            return false;
        }
    }

    /*
    Function : Delete Geofence
    Return : None
    */
    public function delete_geofence($id)
    {
        $request = $this->send_request('geofences/'.$id, 'DELETE', json_encode($id));
        return $request;
    }

    /*
    Function : Get Group
    Return : Group Data
    */
    public function get_group($params)
    {
        $params = http_build_query($params);
        $request = $this->send_request('groups?'.$params, 'GET', json_encode($params));
        if ($request) {
            $result = json_decode($request);
            return $result;
        } else {
            return false;
        }
    }

    /*
    Function : Create Group
    Return : Group ID
    */
    public function create_group($data)
    {
        $request = $this->send_request('groups', 'POST', json_encode($data));
        if ($request) {
            $result = json_decode($request); 
            return $result->id;
        } else {
            return false;
        }
    }

    /*
    Function : Update Group
    Return : Group ID
    */
    public function update_group($id, $data)
    {
        $request = $this->send_request('groups/'.$id, 'PUT', json_encode($data));
        if ($request) {
            $result = json_decode($request);
            return $result;
        } else {
            return false;
        }
    }

    /*
    Function : Delete Group
    Return : None
    */
    public function delete_group($id)
    {
        $request = $this->send_request('groups/'.$id, 'DELETE', json_encode($id));
        return $request;
    }

    /*
    Function : Report Events
    Return : Events
    */
    public function report_events($from, $to, $type = null, $deviceId = null, $groups = array())
    {
        $params = array(
            'from' => str_replace(' ','T',$from).'Z',
            'to' => str_replace(' ','T',$to).'Z'
        );
        if (!is_null($type)) {
            array_push($params, ['type' => $type]);
        }
        if (!is_null($deviceId)) {
            $params['deviceId'] = $deviceId;
        }
        if (count($groups) > 0) {
            array_push($params, $groups);
        }
        $params = http_build_query($params);
        $request = $this->send_request('reports/events?'.$params, 'GET', json_encode($params));
        if ($request) {
            $result = json_decode($request);
            return $result;
        } else {
            return false;
        }
    }

    /*
    Function : Report Trips
    Return : Trips
    */
    public function report_trips($from, $to, $deviceId = null, $groups = array())
    {
        $params = array(
            'from' => str_replace(' ','T',$from).'Z',
            'to' => str_replace(' ','T',$to).'Z'
        );
        if (!is_null($deviceId)) {
            $params['deviceId'] = $deviceId;
        }
        if (count($groups) > 0) {
            array_push($params, $groups);
        }
        $params = http_build_query($params);
        $request = $this->send_request('reports/trips?'.$params, 'GET', json_encode($params));
        if ($request) {
            $result = json_decode($request);
            return $result;
        } else {
            return false;
        }
    }

    /*
    Function : Report Stops
    Return : Stops
    */
    public function report_stops($from, $to, $deviceId = null, $groups = array())
    {
        $params = array(
            'from' => str_replace(' ','T',$from).'Z',
            'to' => str_replace(' ','T',$to).'Z'
        );
        if (!is_null($deviceId)) {
            $params['deviceId'] = $deviceId;
        }
        if (count($groups) > 0) {
            array_push($params, $groups);
        }
        $params = http_build_query($params);
        $request = $this->send_request('reports/stops?'.$params, 'GET', json_encode($params));
        if ($request) {
            $result = json_decode($request);
            return $result;
        } else {
            return false;
        }
    }

    /*
    Function : Report Route
    Return : Route
    */
    public function report_route($from, $to, $deviceId = null, $groups = array())
    {
        $params = array(
            'from' => str_replace(' ','T',$from).'Z',
            'to' => str_replace(' ','T',$to).'Z'
        );
        if (!is_null($deviceId)) {
            $params['deviceId'] = $deviceId;
        }
        if (count($groups) > 0) {
            array_push($params, $groups);
        }
        $params = http_build_query($params);
        $request = $this->send_request('reports/route?'.$params, 'GET', json_encode($params));
        if ($request) {
            $result = json_decode($request);
            return $result;
        } else {
            return false;
        }
    }

    /*
    Function : Report Summary
    Return : Summary
    */
    public function report_summary($from, $to, $deviceId = null, $groups = array())
    {
        $params = array(
            'from' => str_replace(' ','T',$from).'Z',
            'to' => str_replace(' ','T',$to).'Z'
        );
        if (!is_null($deviceId)) {
            $params['deviceId'] = $deviceId;
        }
        if (count($groups) > 0) {
            array_push($params, $groups);
        }
        $params = http_build_query($params);
        $request = $this->send_request('reports/summary?'.$params, 'GET', json_encode($params));
        if ($request) {
            $result = json_decode($request);
            return $result;
        } else {
            return false;
        }
    }

}
