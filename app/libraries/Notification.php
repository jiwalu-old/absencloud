<?php (defined('BASEPATH')) OR exit('No direct script access allowed');

/**
* Jiwalu Framework
* A framework for PHP development
*
* @package     Jiwalu Framework
* @author      Jiwalu Studio
* @copyright   Copyright (c) 2019, Jiwalu Studio (https://www.jiwalu.id)
*/
 
class Notification {
    
    
    public function send_web($fields, $url){
        
        $fields_string = '';
        foreach($fields as $key=>$value) { $fields_string .= $key.'='.$value.'&'; }
        rtrim($fields_string, '&');

        $ch = curl_init();

        curl_setopt($ch,CURLOPT_URL, $url);
        curl_setopt($ch,CURLOPT_VERBOSE, true);
        curl_setopt($ch,CURLOPT_POST, count($fields));
        curl_setopt($ch,CURLOPT_POSTFIELDS, $fields_string);
        curl_setopt($ch,CURLOPT_RETURNTRANSFER, true);

        $result = curl_exec($ch); 
        curl_close($ch); 
    }   
    
    public function send_email($email, $subject, $body){ 
        $curl = curl_init();
         
        curl_setopt_array($curl, array(
            CURLOPT_URL => 'https://api.sendgrid.com/v3/mail/send',
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'POST',
            CURLOPT_POSTFIELDS => '{ 
                "personalizations": [
                    {
                        "to": [
                            {
                                "email": "'.$email.'"
                            }
                        ],
                        "dynamic_template_data": {
                            "dear":"'.$email.'",
                            "subject": "'.$subject.'",
                            "body":"'.$body.'"
                        }
                    }
                ],
                "from": {
                    "email": "noreply@jiwalu.id"
                },
                "template_id": "d-dfe33f9a669d43d5b79c5cd67692f646"
            }',
            CURLOPT_HTTPHEADER => array(
                "Authorization: Bearer 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",
                "Content-Type: application/json",
                "cache-control: no-cache"
            ),
        ));

        $response = curl_exec($curl);
        
        $err = curl_error($curl);
       
        curl_close($curl);
    }

    public function send_sms($to,$content){
        $CI =& get_instance();
        $CI->load->library('twilio');    
        if(substr($to,0,2)=='08'){ 
            $to = '+62'.substr($to,1,15);
        }
        
		$message = $content;
        $response = $CI->twilio->sms($to, $message);
        
        if($response->IsError) 
        echo 'Error: ' . $response->ErrorMessage; 
    }
    
    public function send_whatsapp(){
         //Function For Send Whatsapp
    }

    public function send_telegram(){
         //Function For Send Telegram
    }
 
 
}
