<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
Mobile Network Code
Author : Jiwalu Studio
*/

$config['mnc'] = array(
  '510' => array(
    'country_name' => 'Indonesia',
    'country_uid' => 'id',
    'country_code' => '62',
    'networks' => array(
      '08' => 'Axis/Natrindo',
      '99' => 'Esia (PT Bakrie Telecom) (CDMA)',
      '07' => 'Flexi (PT Telkom) (CDMA)',
      '89' => 'H3G CP',
      '21' => 'Indosat/Satelindo/M3',
      '01' => 'Indosat/Satelindo/M3',
      '00' => 'PT Pasifik Satelit Nusantara (PSN)',
      '27' => 'PT Sampoerna Telekomunikasi Indonesia (STI)',
      '09' => 'PT Smartfren Telecom Tbk',
      '28' => 'PT Smartfren Telecom Tbk',
      '11' => 'PT. Excelcom',
      '10' => 'Telkomsel'
    )
  )
);