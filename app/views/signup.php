<!DOCTYPE html>
<html class="loading" lang="en" data-textdirection="ltr">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <meta name="author" content="Jiwalu Studio">
    <title>Register - AbsenCloud</title>
    <link rel="apple-touch-icon" href="<?php echo base_url(); ?>app-assets/images/ico/apple-icon-120.png">
    <link rel="shortcut icon" type="image/png" href="<?php echo base_url(); ?>app-assets/images/absen-mini.png">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,500,600" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>app-assets/vendors/css/vendors.min.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>app-assets/css/bootstrap.min.css?v=<?php echo date('YmdHis'); ?>">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>app-assets/css/bootstrap-extended.min.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>app-assets/css/colors.min.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>app-assets/css/components.min.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>app-assets/css/core/menu/menu-types/vertical-menu.min.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>app-assets/css/core/colors/palette-gradient.min.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>app-assets/css/pages/auth.min.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>app-assets/css/style.min.css?v=<?php echo date('YmdHis'); ?>">
  </head>
  <body
    class="vertical-layout vertical-menu-modern 1-column  navbar-floating footer-static bg-full-screen-image  blank-page blank-page"
    data-open="click" data-menu="vertical-menu-modern" data-col="1-column">
    <div class="app-content content" style="background-color: gainsboro;">
      <div class="content-wrapper">
        <div class="content-header row">
        </div>
        <div class="content-body" align="center">
         
        <form id="form" action="<?php echo site_url('auth/register'); ?>" method="post" enctype="multipart/form-data">
 
          <div class="card col-md-6" align="left">
              <div class="card-header"> 
                  <h5>Register and start using Absent Cloud now!</h5>                      
              </div>

              <div class="card-body">
                  <div id="form-alert"><?php echo ($this->session->flashdata('message')) ? $this->session->flashdata('message') : ''; ?></div>
                 
                  <div class="row">
                    <div class="col-sm-12"> 

                      <div class="form-group required">
                        <label>Fullname</label>
                        <input class="form-control" type="text" name="name"  >
                      </div>
                      <div class="form-group required">
                        <label>Username</label>
                        <input class="form-control" type="text" name="username"  >
                      </div>
                      <div class="form-group required">
                        <label>Password</label>
                        <input class="form-control" type="password" name="password" id="password" >
                      </div>
                      <div class="form-group required">
                        <label for="passcon">Confirm Password</label>
                        <div class="input-group"> 
                          <input class="form-control" type="password" id="passcon" name="passwordconf"  >
                          <div class="input-group-append">
                            <span class="input-group-text" id="check"></span>
                          </div>
                        </div>
                      </div>
                      
                      <div class="form-group required" align="center">
                        <label >
                          <h5>Company Data</h5>
                        </label> 
                      </div>
                      <div class="form-group required">
                        <label>Company Name</label>
                        <input class="form-control" type="text" name="company_name"  >
                      </div>
                      <div class="form-group required">
                        <label>Bisiness Field</label>
                        <input class="form-control" type="text" name="business_field"  >
                      </div>
                      <div class="form-group required">
                        <label>Province</label>
                        <input class="form-control" type="text" name="province"  >
                      </div>
                      <div class="form-group required">
                        <label>City / Regency</label>
                        <input class="form-control" type="text" name="city"  >
                      </div>
                      <div class="form-group required">
                        <label>Address</label>
                        <textarea name="address" class="form-control"></textarea>
                      </div>
                      <div class="form-group required">
                        <label>Phone</label>
                        <input class="form-control" type="text" name="phone"  >
                      </div>
                      <div class="form-group required" align="center">
                        <button type="submit" class="btn btn-primary waves-effect waves-light round">Create Account</button>
                      </div>

                    </div>
                  </div> 
              </div>
 
          </div>

      </form>

        </div>
      </div>
    </div>
    <div id="login-loading" style="display:none">
      <div class="spinner-border text-primary" role="status">
        <span class="sr-only">Loading...</span>
      </div>
    </div>
    <script src="<?php echo base_url(); ?>app-assets/vendors/js/vendors.min.js"></script>
    <script src="<?php echo base_url(); ?>app-assets/vendors/js/forms/jquery.form.min.js"></script>
    <script src="<?php echo base_url(); ?>app-assets/vendors/js/forms/validation/jquery.validate.min.js"></script>
    <script src="<?php echo base_url(); ?>app-assets/js/core/app-menu.min.js"></script>
    <script src="<?php echo base_url(); ?>app-assets/js/core/app.min.js"></script>
    <script src="<?php echo base_url(); ?>app-assets/js/scripts/components.min.js"></script>
    <script>
      $(function () {
        $("#passcon").on('input', function(){
          pass = $("#password").val();
          con = $("#passcon").val();
          if(con==pass){
            $("#check").html('✔').css('color','green');
          }else{
            $("#check").html('✘').css('color','red');
          } 
        });

        $("#form-login").validate({
          errorElement: "span",
          errorClass: 'help-block',
          highlight: function (element) {
            $(element).parent().addClass('error');
          },
          unhighlight: function (element) {
            $(element).parent().removeClass('error');
          },
          submitHandler: function (form) {
            $('#login-loading').show();
            $(form).ajaxSubmit({
              target: '#login-alert',
              success: function (data) {
                $('#login-loading').hide();
              },
              error: function (data) {
                $('#login-loading').hide();
              }
            });
          }
        });
      });
    </script>
  </body>
</html>