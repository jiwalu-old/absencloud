<div class="app-content content">
    <div class="content-wrapper">
        <div class="content-header row">
        </div>
        <div class="content-body"> 
            <section class="row flexbox-container">
                <div class="col-xl-7 col-md-8 col-12 d-flex justify-content-center">
                    <div class="card auth-card bg-transparent shadow-none rounded-0 mb-0 w-100">
                        <div class="card-content">
                            <div class="card-body text-center">
                                <img src="<?php echo base_url('app-assets/images/not-authorized.png');?>" class="img-fluid align-self-center" alt="branding logo">
                                <h1 class="font-large-1 my-1">Your link has expired</h1>
                                
                            </div>
                        </div>
                    </div>
                </div>
            </section> 

        </div>
    </div>
</div>