<!DOCTYPE html>
<html class="loading" lang="en" data-textdirection="ltr">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
		<meta name="author" content="Jiwalu Studio">
		<title><?php echo $title; ?> | AbsenCloud</title>
		<link rel="apple-touch-icon" href="<?php echo base_url('app-assets/images/ico/apple-icon-120.png'); ?>">
		<link rel="shortcut icon" type="image/png" href="<?php echo base_url('app-assets/images/absen-mini.png'); ?>">
		<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,500,600" rel="stylesheet">
		
		<link rel="stylesheet" type="text/css" href="<?php echo base_url('app-assets/vendors/dropify/dist/css/dropify.min.css'); ?>">
		<link rel="stylesheet" type="text/css" href="<?php echo base_url('app-assets/vendors/css/vendors.min.css'); ?>">
		<link rel="stylesheet" type="text/css" href="<?php echo base_url('app-assets/vendors/css/tables/datatable/datatables.min.css'); ?>">
		<link rel="stylesheet" type="text/css" href="<?php echo base_url('app-assets/vendors/css/pickers/pickadate/pickadate.css'); ?>">
		<link rel="stylesheet" type="text/css" href="<?php echo base_url('app-assets/vendors/css/extensions/toastr.css'); ?>">
		<link rel="stylesheet" type="text/css" href="<?php echo base_url('app-assets/vendors/css/forms/select/select2.css'); ?>">
		<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/css/bootstrap-datetimepicker.css">
		<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/css/bootstrap-datetimepicker.min.css">
	
		<link rel="stylesheet" type="text/css" href="<?php echo base_url('app-assets/css/bootstrap.min.css'); ?>">
		<link rel="stylesheet" type="text/css" href="<?php echo base_url('app-assets/css/bootstrap-extended.min.css?v='.date('YmdHis')); ?>">
		<link rel="stylesheet" type="text/css" href="<?php echo base_url('app-assets/css/colors.min.css'); ?>">
		<link rel="stylesheet" type="text/css" href="<?php echo base_url('app-assets/css/components.min.css?v='.date('YmdHis')); ?>">
		<link rel="stylesheet" type="text/css" href="<?php echo base_url('app-assets/css/plugins/extensions/toastr.min.css'); ?>">
		<link rel="stylesheet" type="text/css" href="<?php echo base_url('app-assets/css/core/menu/menu-types/vertical-menu.min.css'); ?>">
		<link rel="stylesheet" type="text/css" href="<?php echo base_url('app-assets/css/core/colors/palette-gradient.min.css'); ?>">
		<link rel="stylesheet" type="text/css" href="<?php echo base_url('app-assets/css/style.min.css?v='.date('YmdHis')); ?>">
		<?php
		foreach ($css as $file) {
			echo "\n    ";
			echo '<link rel="stylesheet" type="text/css" href="'.$file.'">';
		} echo "\n";
		?>

		<script>
			var base_url = '<?php echo base_url(); ?>';
		</script>
		<script src="<?php echo base_url('app-assets/vendors/js/vendors.min.js'); ?>"></script>
	</head>

	<body class="vertical-layout vertical-menu-modern 2-columns navbar-sticky <?php echo ($module['name'] == 'tracking' || $module['name'] == 'monitoring' || $module['name'] == 'history')?'menu-collapsed':''; ?>" data-open="click" data-menu="vertical-menu-modern" data-col="2-columns">
		<?php $this->load->view('sidebar'); ?>
		<div class="app-content content">
			<div class="content-overlay"></div>
			<?php echo $this->load->view('header'); ?>
			<div id="content-container" class="content-wrapper">
				<?php echo $output; ?>
			</div>
		</div>

		<div class="sidenav-overlay"></div>
		<div class="drag-target"></div>

		<script src="<?php echo base_url('app-assets/vendors/dropify/dist/js/dropify.min.js'); ?>"></script>
		<script src="<?php echo base_url('app-assets/vendors/js/tables/datatable/datatables.min.js'); ?>"></script>
		<script src="<?php echo base_url('app-assets/vendors/js/tables/datatable/datatables.bootstrap4.min.js'); ?>"></script>
		<script src="<?php echo base_url('app-assets/vendors/js/pickers/pickadate/picker.js'); ?>"></script>
		<script src="<?php echo base_url('app-assets/vendors/js/pickers/pickadate/picker.date.js'); ?>"></script>
		<script src="<?php echo base_url('app-assets/vendors/js/forms/jquery.form.min.js'); ?>"></script>
		<script src="<?php echo base_url('app-assets/vendors/js/forms/validation/jquery.validate.min.js'); ?>"></script>
		<script src="<?php echo base_url('app-assets/vendors/js/extensions/toastr.min.js'); ?>"></script>
		<script src="<?php echo base_url('app-assets/vendors/js/forms/select/select2.js'); ?>"></script>
		<script src="<?php echo base_url('app-assets/js/core/app-menu.min.js'); ?>"></script>
		<script src="<?php echo base_url('app-assets/js/core/app.min.js'); ?>"></script>
		<script src="<?php echo base_url('app-assets/js/scripts/components.min.js'); ?>"></script>
		<script src="<?php echo base_url('app-assets/js/scripts/grid.min.js?v='.date('YmdHis')); ?>"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/js/bootstrap-datetimepicker.min.js"></script>
		<?php
		foreach ($js as $file) {
			echo "\n    ";
			echo '<script src="'.$file.'"></script>';
		} echo "\n";
		?>
		<script type="text/javascript">
		  
		$(function() {
			var i = 0;
			 
			$('body').on('click', '.ajax-load', function(e) {
				e.preventDefault();
				var url = $(this).attr('href');
				var title = $(this).attr('title');
				$('#content-container').load(url);
				document.title = title + ' | AbsenCloud';
				window.history.pushState("", title, url);
				$(this).parent().siblings().removeClass('active');
				$(this).parent().siblings().find('ul').hide();
				$(this).parent().siblings().find('li').removeClass('active');
				$(this).parent().addClass('active');
				$(this).parent().parent().parent().siblings().removeClass('active');
				$(this).parent().parent().parent().siblings().find('li').removeClass('active');
				$(this).parent().parent().parent().addClass('active');
			});
		});

		</script>
	</body>
</html>