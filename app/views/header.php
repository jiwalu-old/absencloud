
<div class="header-navbar-shadow"></div>
<nav class="header-navbar navbar-expand-lg navbar navbar-with-menu fixed-top navbar-light navbar-shadow">
	<div class="navbar-wrapper">
		<div class="navbar-container content">
			<div class="navbar-collapse" id="navbar-mobile">
				<div class="mr-auto float-left bookmark-wrapper d-flex align-items-center">
					<ul class="nav navbar-nav">
						<li class="nav-item mobile-menu d-xl-none mr-auto">
							<a class="nav-link nav-menu-main menu-toggle hidden-xs" href="#">
								<i class="ficon feather icon-menu"></i>
							</a>
						</li>
					</ul>
					<ul class="nav navbar-nav bookmark-icons">
						<li class="nav-item d-none d-lg-block">
							<strong><?php echo userinfo('company_name'); ?></strong>
						</li>
					</ul>
				</div>
				<ul class="nav navbar-nav float-right">
					 
					<li class="dropdown dropdown-notification nav-item">
						<a class="nav-link nav-link-label" href="#" data-toggle="dropdown">
							<i class="ficon feather icon-bell"></i>
							<span class="badge badge-pill badge-primary badge-up" id="totalnotif"></span>
						</a>
						<ul class="dropdown-menu dropdown-menu-media dropdown-menu-right">
							<li class="dropdown-menu-header">
								<div class="dropdown-header m-0 p-2">
									<h3 class="white">Notifications</h3><span class="notification-title"></span>
								</div>
							</li>
							<li class="scrollable-container media-list ps ps--active-y" id='content-notif'> </li>
							<li class="dropdown-menu-footer"><a class="dropdown-item p-1 text-center"
								href="<?php echo site_url('notification'); ?>">View all notifications</a>
							</li>
						</ul>
					</li>
					  
					<li class="dropdown dropdown-user nav-item"><a class="dropdown-toggle nav-link dropdown-user-link" href="#"
							data-toggle="dropdown">
							<div class="user-nav d-sm-flex d-none">
								<span class="user-name text-bold-600"><?php echo userinfo('name'); ?></span>
							</div>
							<span>
								<?php $avatar = (!empty(userinfo('image'))) ? base_url().'app-assets/images/user/'.userinfo('image') : base_url('app-assets/images/portrait/small/avatar-s-1.png'); ?>
								<img src="<?php echo $avatar; ?>" alt="profile" id="avatarprofile" width="40" height="40" class="round">
							</span>
						</a>
						<div class="dropdown-menu dropdown-menu-right">
							<a class="dropdown-item" href="<?php echo site_url('user/profile'); ?>">
								<i class="feather icon-user"></i> Edit Profile
							</a>
							<div class="dropdown-divider"></div>
							<a class="dropdown-item" href="<?php echo site_url('auth/logout'); ?>">
								<i class="feather icon-power"></i> Logout
							</a>
						</div>
					</li>
				</ul>
			</div>
		</div>
	</div>
</nav>

<script src="<?php echo base_url('app-assets/js/pusher.min.js'); ?>"></script>
<script src="<?php echo base_url('app-assets/js/scripts/dashboard.js'); ?>"></script>
<script src="<?php echo base_url('app-assets/vendors/js/extensions/moment.min.js'); ?>"></script> 

<script type="text/javascript">
var siteurl = "<?php echo site_url('dashboard');?>";
</script>