<div class="main-menu menu-fixed menu-light menu-accordion menu-shadow" data-scroll-to-active="true">
  <div class="navbar-header">
    <ul class="nav navbar-nav flex-row">
      <li class="nav-item mr-auto">
        <a class="navbar-brand" href="<?php echo base_url(); ?>">
          <img src="<?php echo base_url(); ?>app-assets/images/absen.png" class="w-100">
        </a>
      </li>
      <li class="nav-item nav-toggle">
        <a class="nav-link modern-nav-toggle pr-0" data-toggle="collapse">
          <i class="feather icon-x d-block d-xl-none font-medium-4 primary toggle-icon"></i>
          <i class="toggle-icon feather icon-disc font-medium-4 d-none d-xl-block collapse-toggle-icon primary" data-ticon="icon-disc"></i>
        </a>
      </li>
    </ul>
  </div>
  <div class="shadow-bottom"></div>
  <div class="main-menu-content">
    <ul class="navigation navigation-main" id="main-menu-navigation" data-menu="menu-navigation">
      <li class="navigation-header"><span>General</span></li>
      <?php 
        $group = userinfo('group_id');
        $privilage = $this->db->query("SELECT privileges::json->'menu' as privilage from app.users u inner join app.user_groups g on u.group_id=g.id where g.id='$group'")->row('privilage');
        $idMenu = str_replace(['[', '"', ']'], '', $privilage);
        if($idMenu==''){
          $andId = 'and id!=id';
        }else{
          $andId = "and id in ($idMenu)";
        }

        $qMenu = $this->db->query("SELECT * from app.menus where parent is null $andId order by sort_order,parent");
        foreach($qMenu->result()as $row){ 
          if($row->name=='dashboard'){
            $icon = 'home';
          }elseif($row->name=='company'){
            $icon = 'briefcase';
          }elseif($row->name=='personnel'){
            $icon = 'users';
          }elseif($row->name=='attendance_spot'){
            $icon = 'map-pin';
          }else{
            $icon = '';
          }
      ?>

        <li class="nav-item <?php echo ($menu['menu'] == $row->name)?'active':''; ?>">
          <a href="<?php echo site_url($row->url); ?>">
            <?php if($row->parent==''){
              echo "<i class='feather icon-$icon'></i>";
            }?>           
            <span class="menu-title" title="<?php echo $row->title;?>"><?php echo $row->title;?></span>
          </a>

          <?php $sub = $this->db->query("SELECT * from app.menus where parent = $row->id $andId order by sort_order");
            $check = $sub->num_rows();
            if($check>0){
          ?>
              <ul class="menu-content">
                <?php foreach($sub->result()as $key){?>
                <li class="<?php echo ($menu['submenu'] == $key->name)?'active':''; ?>">
                  <a href="<?php echo site_url($key->url); ?>" class="ajax-load" title="<?php echo $key->title;?>">
                    <?php echo $key->title;?>
                  </a>
                </li>
                <?php }?>
              </ul>
            <?php }?>
        </li>

      <?php };?>
    </ul>
  </div>
  </div>
 