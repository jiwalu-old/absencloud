<!DOCTYPE html>
<html class="loading" lang="en" data-textdirection="ltr">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <meta name="author" content="Jiwalu Studio">
    <title>Login - AbsenCloud</title>
    <link rel="apple-touch-icon" href="<?php echo base_url(); ?>app-assets/images/ico/apple-icon-120.png">
    <link rel="shortcut icon" type="image/png" href="<?php echo base_url(); ?>app-assets/images/absen-mini.png">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,500,600" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>app-assets/vendors/css/vendors.min.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>app-assets/css/bootstrap.min.css?v=<?php echo date('YmdHis'); ?>">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>app-assets/css/bootstrap-extended.min.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>app-assets/css/colors.min.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>app-assets/css/components.min.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>app-assets/css/core/menu/menu-types/vertical-menu.min.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>app-assets/css/core/colors/palette-gradient.min.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>app-assets/css/pages/auth.min.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>app-assets/css/style.min.css?v=<?php echo date('YmdHis'); ?>">
  </head>
  <body
    class="vertical-layout vertical-menu-modern 1-column  navbar-floating footer-static bg-full-screen-image  blank-page blank-page"
    data-open="click" data-menu="vertical-menu-modern" data-col="1-column">
    <div class="app-content content">
      <div class="content-wrapper">
        <div class="content-header row">
        </div>
        <div class="content-body">
          <section class="row flexbox-container">
            <div class="col-xl-8 col-11 d-flex justify-content-center">
              <div class="card bg-authentication rounded-0 mb-0">
                <div class="row m-0">
                  <div class="col-lg-6 d-lg-block d-none text-center align-self-center px-1 py-0">
                    <img src="<?php echo base_url(); ?>app-assets/images/pages/login.png" alt="branding logo">
                  </div>
                  <div class="col-lg-6 col-12 p-0">
                    <div class="card rounded-0 mb-0 px-2">
                      <div class="card-header pb-1">
                        <div class="card-title">
                          <h4 class="mb-0">Login</h4>
                        </div>
                      </div>
                      <p class="px-2">Welcome back, please login to your account.</p>
                      <div id="login-alert" class="px-2"></div>
                      <div class="card-content">
                        <div class="card-body pt-1">
                          <form id="form-login" action="<?php echo current_url(); ?>" method="post">
                            <input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>">
                            <input type="hidden" name="redirect" value="<?php echo $this->session->flashdata('redirect'); ?>"> 
                            <div><?php  echo $this->session->flashdata('register');?></div>
                            <fieldset class="form-label-group form-group position-relative has-icon-left">
                              <input type="text" class="form-control" name="login_company" id="company-code" placeholder="Company Code" required>
                              <div class="form-control-position">
                                <i class="feather icon-at-sign"></i>
                              </div>
                              <label for="company-code">Company Code</label>
                            </fieldset>

                            <fieldset class="form-label-group form-group position-relative has-icon-left">
                              <input type="text" class="form-control" name="login_user" id="user-name" placeholder="Username" required>
                              <div class="form-control-position">
                                <i class="feather icon-user"></i>
                              </div>
                              <label for="user-name">Username</label>
                            </fieldset>

                            <fieldset class="form-label-group form-group position-relative has-icon-left">
                              <input type="password" class="form-control" name="login_pass" id="user-password" placeholder="Password" required>
                              <div class="form-control-position">
                                <i class="feather icon-lock"></i>
                              </div>
                              <label for="user-password">Password</label>
                            </fieldset>
                            <div class="form-group d-flex justify-content-between align-items-center">
                              <div class="text-left">
                                <fieldset class="checkbox">
                                  <div class="vs-checkbox-con vs-checkbox-primary">
                                    <input type="checkbox">
                                    <span class="vs-checkbox">
                                      <span class="vs-checkbox--check">
                                        <i class="vs-icon feather icon-check"></i>
                                      </span>
                                    </span>
                                    <span class="">Remember me</span>
                                  </div>
                                </fieldset>
                              </div>
                            </div>
                            <button type="submit" class="btn btn-primary">Login</button>
  
                            <a href="<?php echo base_url('signup');?>" class="btn btn-secondary" style="float:right;">Signup</a>
                          </form>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </section>
        </div>
      </div>
    </div>
    <div id="login-loading" style="display:none">
      <div class="spinner-border text-primary" role="status">
        <span class="sr-only">Loading...</span>
      </div>
    </div>
    <script src="<?php echo base_url(); ?>app-assets/vendors/js/vendors.min.js"></script>
    <script src="<?php echo base_url(); ?>app-assets/vendors/js/forms/jquery.form.min.js"></script>
    <script src="<?php echo base_url(); ?>app-assets/vendors/js/forms/validation/jquery.validate.min.js"></script>
    <script src="<?php echo base_url(); ?>app-assets/js/core/app-menu.min.js"></script>
    <script src="<?php echo base_url(); ?>app-assets/js/core/app.min.js"></script>
    <script src="<?php echo base_url(); ?>app-assets/js/scripts/components.min.js"></script>
    <script>
      $(function () {
        $("#form-login").validate({
          errorElement: "span",
          errorClass: 'help-block',
          highlight: function (element) {
            $(element).parent().addClass('error');
          },
          unhighlight: function (element) {
            $(element).parent().removeClass('error');
          },
          submitHandler: function (form) {
            $('#login-loading').show();
            $(form).ajaxSubmit({
              target: '#login-alert',
              success: function (data) {
                $('#login-loading').hide();
              },
              error: function (data) {
                $('#login-loading').hide();
              }
            });
          }
        });
      });
    </script>
  </body>
</html>