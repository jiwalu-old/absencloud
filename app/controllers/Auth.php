<?php defined('BASEPATH') or exit('No direct script access allowed');

class Auth extends CI_Controller
{
  public function __construct()
  {
    parent::__construct();

    $this->load->model('users');
    $this->load->model('user_groups');
    $this->load->model('companies');
  }

  public function index()
  {

  }

  public function signup(){
    $this->load->view('signup');
  }

  public function register(){
    $data = $this->input->post(null, true); 

    $comp['name'] = $data['company_name'];
    $comp['code'] = $data['company_name'];
    $comp['business_field'] = $data['business_field'];
    $comp['province'] = $data['province'];
    $comp['city'] = $data['city'];
    $comp['address'] = $data['address'];
    $comp['phone'] = $data['phone'];

    $ins_comp = $this->companies->insert($comp);

    if (!empty($data['password'])) {
      $this->load->library('password');
      $user['password'] = $this->password->hash_password($data['password']);
    }  
      
    $user['company_id'] = $ins_comp;
    $user['group_id'] = null;
    $user['username'] = $data['username']; 
    $user['name'] = $data['name'];
    $ins_user = $this->users->insert($user);
    $this->session->set_flashdata('register',$this->show_success('Registration Success, Please Login')); 
    redirect('/login'); 
  }

  public function login()
  { 
    header("Expires: Mon, 11 Apr 1988 05:52:00 GMT");
    header("Cache-Control: no-store, no-cache, must-revalidate");

    if ($this->input->is_ajax_request()) {
      $this->form_validation->set_rules('login_company', 'Company Code', 'required');
      $this->form_validation->set_rules('login_user', 'Username', 'required');
      $this->form_validation->set_rules('login_pass', 'Password', 'required');

      if ($this->form_validation->run()) {
        $company_code = $this->input->post('login_company');
        $username = $this->input->post('login_user');
        $password = $this->input->post('login_pass');

        $company = $this->check_company($company_code);

        if ($company) {
          $user = $this->check_user($username, $company->id);
          if ($user) {
            $verify = $this->verify($user, $password);
            if ($verify) {
              if ($this->input->post('redirect')) {
                echo ajaxRedirect($this->input->post('redirect'), 0);
              } else {
                echo ajaxRedirect('/', 0);
              }
            } else {
              $this->session->set_flashdata('message', 'Username and Password did not match');
              echo $this->show_error('Username and Password did not match.');
            }
          } else {
            $this->session->set_flashdata('message', 'Username is not registered');
            echo $this->show_error('Username is not registered');
          }
        } else {
          $this->session->set_flashdata('message', 'Company is not registered');
          echo $this->show_error('Your Company is not registered');
        }
      } else {
        echo $this->show_error(validation_errors());
      }
    } else {
      if ($this->session->userdata('logged_in')) {
        redirect('/', 'refresh');
      }

      $this->output->set_title('Login');

      $this->data['message'] = (validation_errors()) ? $this->show_error(validation_errors()) : $this->show_error($this->session->flashdata('message'));
      if ($this->session->flashdata('redirect')) {
        $this->data['redirect'] = $this->session->flashdata('redirect');
      }

      $this->load->view('login', $this->data);
    }
  }

  public function logout()
  {
    $this->session->unset_userdata(['logged_in', 'login_user']);
    redirect('/');
  }

  private function check_company($company_code)
  {
    $company = $this->companies->get(['code' => $company_code]);
    if ($company) {
      return $company;
    }
    return false;
  }

  private function check_user($username, $company_id)
  {
    $user = $this->users->get(['username' => $username, 'company_id' => $company_id]);
    if ($user) {
      return $user;
    }
    return false;
  }

  private function verify($user, $password)
  {
    $this->load->library('password');
    $verify = $this->password->check_password($password, $user->password);

    if ($verify) {
      $this->users->update(['last_login' => date('Y-m-d H:i:s')], $user->id);
      $this->set_session($user);
      return true;
    }
    return false;
  }

  private function set_session($user) {
    $user_group = $this->user_groups->get($user->group_id);
    $company = $this->companies->get($user->company_id);
    $session_data = array(
      'logged_in' => TRUE,
      'login_user' => [
        'id' => $user->id,
        'group_id' => $user->group_id,
        'company_id' => $user->company_id,
        'group_id' => $user->group_id,
        'company_name' => $company->name,
        'email' => $user->email,
        'name' => $user->name,
        'username' => $user->username,
        'role' => $user_group->name,
        'image' => $user->image
      ]
    );

    $this->session->set_userdata($session_data);

    return true;
  }

  private function show_error($err_message)
  {
    return '<div class="alert alert-danger">
      <button type="button" class="close" data-dismiss="alert"><span>×</span><span class="sr-only">Close</span></button>'.$err_message.'</div>';
  }

  private function show_success($succ_message)
  {
    return '<div class="alert alert-success">
      <button type="button" class="close" data-dismiss="alert"><span>×</span><span class="sr-only">Close</span></button>'.$succ_message.'</div>';
  }

  public function hash($password)
  {
    $this->load->library('password');
    echo $this->password->hash_password($password);
  }
}
