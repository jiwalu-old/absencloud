<?php (defined('BASEPATH')) or exit('No direct script access allowed');

class App extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();

        $this->load->library('user_agent');

        // Turn on IE8-IE9 XSS prevention tools
        $this->output->set_header('X-XSS-Protection: 1; mode=block');
        // Don't allow any pages to be framed - Defends against CSRF
        $this->output->set_header('X-Frame-Options: DENY');
        // prevent mime based attacks
        $this->output->set_header('X-Content-Type-Options: nosniff');
        // $this->output->enable_profiler(TRUE);

        if (!$this->is_https()) {
            // redirect('https://'.@$_SERVER['HTTP_HOST'].'/'.uri_string());
        }

        if (!is_login() && $this->uri->segment(2) != 'login') {
            if (!$this->input->is_ajax_request()) {
                redirect('login?redirect='.urlencode(uri_string()));
            } else {
                echo '<script>window.location="'.site_url('login?redirect='.urlencode(uri_string())).'";</script>';
            }
            exit();
        }

        $this->data = array();
    }

    public function privileged($roles = array())
    {
        if (!in_array(userinfo('role'), $roles)) {
            $this->session->set_flashdata('message', errorMessage('Permission denied!'));
            if (!$this->input->is_ajax_request()) {
                redirect('dashboard');
            } else {
                echo '<script>window.location="'.site_url('dashboard').'";</script>';
            }
            exit();
        }
    }

    public function is_https()
    {
        if (isset($_SERVER['HTTPS']) and $_SERVER['HTTPS'] == 1) {
            return TRUE;
        } elseif (isset($_SERVER['HTTPS']) and $_SERVER['HTTPS'] == 'on') {
            return TRUE;
        } else {
            return FALSE;
        }
    }
}