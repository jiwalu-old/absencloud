<?php defined('BASEPATH') or exit('No direct script access allowed');

class Dashboard extends App
{
    function __construct()
    {
        parent::__construct();
  
        $this->load->model('user_groups');
        $this->load->model('notifications');

        $this->data = [
            'module' => [
                'name'     => 'dashboard',
                'title'    => 'Dashboard',
                'icon'     => 'icon-files-empty',
                'url'      => site_url('dashboard')
            ],
            'menu' => ['menu' => 'dashboard', 'submenu' => ''],
        ];
    }

    public function index()
    {
        if (!$this->input->is_ajax_request()) {
            $this->output->set_template('default');
        } 
         
        $this->output->set_title('Dashboard');
         
        $this->load->view('dashboard', $this->data);
    }

    function getData(){ 
        $group_id = userinfo('group_id');
        $company_id = userinfo('company_id');
        
        $vehicle = json_decode($this->user_groups->get_vehicle($group_id));
        if($vehicle){
            
            $return['status']='success';
            $getPie = $this->vehicles_read->getPie()[0];
            $return['total_vehicle'] = (int)$getPie['total_vehicle'];
            $return['pie'][] = (int)$getPie['in_geofence'];
            $return['pie'][] = (int)$getPie['out_geofence'];
 
            $getBar = $this->vehicles_read->getBar();
            foreach($getBar as $key=> $value){
                $return['distance'][] = format_number($value->total_distance/1000);
                $return['name'][] = $value->name;
            }
             
        }else{
            $return['status']='failed';
        }
         
        

        echo json_encode($return);
    }
 

    public function notfound()
    {
        if (!$this->input->is_ajax_request()) {
            $this->output->set_template('default');
        }

        $this->output->set_title('404 Page Not Found');

        $this->load->view('404', $this->data);
    }
    public function coming_soon()
    {
        if (!$this->input->is_ajax_request()) {
            $this->output->set_template('default');
        }

        $this->output->set_title('Coming Soon');

        $this->load->view('coming_soon', $this->data);
    }

    public function expired()
    {
        if (!$this->input->is_ajax_request()) {
            $this->output->set_template('default');
        }

        $this->output->set_title('Your link has expired');

        $this->load->view('expired', $this->data);
    }

    function getNotif(){
        $return=array(); 
        $process = $this->notifications->get_all(['company_id'=>userinfo('company_id')]);
        if($process){
            foreach($process as $value){ 
                $return[]=array(
                    'id'=> $value->id,
                    'title'=>$value->title,
                    'is_read'=>$value->is_read,
                    'url'=>$value->url,
                    'notiftime'=>$value->created_at
                );
            }
           
            echo json_encode($return);
        }else{
            echo false;
        } 
    }

    function updateNotif(){
        $id = $this->input->get('id');
        $process = $this->notifications->update(['is_read'=>'t'],['id'=>$id]);
        $valUrl = $this->notifications->fields('url')->get($id);

        require_once(APPPATH.'third_party/vendor/autoload.php'); 
        $options = array(
            'cluster' => 'ap1',
            'useTLS' => true
          );
          $pusher = new Pusher\Pusher(
            '4c14965b64bb3e5ebdc9',
            'fa184649e50aee9bdd6f',
            '919243',
            $options
          );
         
          $data['value']['url'] = $valUrl;
          $pusher->trigger('absen-channel', 'my-event', $data);
    }
}
