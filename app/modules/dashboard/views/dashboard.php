<div class="content-header">
	<div class="content-header-left mb-2">
		<div class="breadcrumbs-top">
			<h2 class="content-header-title mb-0"><?php echo $module['title']; ?></h2>
		</div>
	</div>
</div>
<div class="content-body">	
	<div class="row">
		<div class="col-md-6">
			<div class="card">
				 
			</div>
		</div>

		
		<div class="col-md-6">
			<div class="card">
			</div>
		</div>
	</div>
</div>
 
<?php $this->load->css('app-assets/vendors/css/charts/apexcharts.css'); ?>
<?php $this->load->js('app-assets/vendors/js/charts/apexcharts.min.js'); ?> 
 
<script>
var module = {
  url: "<?php echo $module['url']; ?>",
	hash: "<?php echo $this->security->get_csrf_hash(); ?>"
} 
 
</script>

<style type="text/css">
	* {
	-webkit-box-sizing: border-box;
			box-sizing: border-box;
	}

	 
	#notfound {
		position: relative;
		min-height: 325px
	}

	#notfound .notfound {
	position: absolute;
	left: 50%;
	top: 50%;
	-webkit-transform: translate(-50%, -50%);
		-ms-transform: translate(-50%, -50%);
			transform: translate(-50%, -50%);
	}

	.notfound {
	max-width: 520px;
	width: 100%;
	line-height: 1.4;
	text-align: center;
	}

	.notfound .notfound-404 {
	position: relative;
	height: 200px;
	margin: 0px auto 20px;
	z-index: -1;
	}

	.notfound .notfound-404 h1 {
		font-family: 'Montserrat', sans-serif;
		font-size: 136px;
		font-weight: 200;
		margin: 0px;
		color: #3e6fb7;
		text-transform: uppercase;
		position: absolute;
		left: 50%;
		top: 50%;
		-webkit-transform: translate(-50%, -50%);
		-ms-transform: translate(-50%, -50%);
		transform: translate(-50%, -50%);
	}

	.notfound .notfound-404 h2 {
		font-family: 'Montserrat', sans-serif;
		font-size: 26px;
		font-weight: 400;
		text-transform: uppercase;
		color:#e68525;
		background:#fff;
		padding: 25px 5px;
		margin: auto;
		display: inline-block;
		position: absolute;
		bottom: 0px;
		left: 0;
		right: 0;
		width: 300px;
	}

	.notfound a {
	font-family: 'Montserrat', sans-serif;
	display: inline-block;
	font-weight: 700;
	text-decoration: none;
	color: #fff;
	text-transform: uppercase;
	padding: 13px 23px;
	background: #ff6300;
	font-size: 18px;
	-webkit-transition: 0.2s all;
	transition: 0.2s all;
	}

	.notfound a:hover {
	color: #ff6300;
	background: #211b19;
	}

	@media only screen and (max-width: 767px) {
	.notfound .notfound-404 h1 {
		font-size: 148px;
	}
	}

	@media only screen and (max-width: 480px) {
	.notfound .notfound-404 {
		height: 148px;
		margin: 0px auto 10px;
	}
	.notfound .notfound-404 h1 {
		font-size: 86px;
	}
	.notfound .notfound-404 h2 {
		font-size: 16px;
	}
	.notfound a {
		padding: 7px 15px;
		font-size: 14px;
	}
	}

</style>