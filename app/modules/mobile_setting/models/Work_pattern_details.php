<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Work_pattern_details extends MY_Model {

  public $table = 'work_pattern_detail';
  public $primary_key = 'id';

  public function __construct()
  {
    parent::__construct();
    $this->soft_deletes = FALSE;
  } 

  public function getvalue($id){
    $this->db->select('*');
    $this->db->from($this->table);
    $this->db->where('id',$id);
  
    $query = $this->db->get();
    return $query->result_array();
  }
 
}