<?php defined('BASEPATH') or exit('No direct script access allowed!');

class Work_pattern extends App
{

  public function __construct()
  {
    parent::__construct();
 
    $this->load->model('work_patterns');
    $this->load->model('work_pattern_details');

    $this->data = [
      'module' => [
        'name' => 'work_pattern',
        'title' => 'Work Pattern',
        'title_pl' => 'Work Pattern',
        'url' => site_url('mobile_setting/work_pattern')
      ],
      'menu' => ['menu' => 'mobile_setting', 'submenu' => 'work_pattern'],
      'error_message' => $this->session->flashdata('error_message'),
      'success_message' => $this->session->flashdata('success_message'),
      
    ];
  }

  public function index()
  {
    $this->privileged(['superadmin', 'admin']);
    if (!$this->input->is_ajax_request()) {
      $this->output->set_template('default');
      $this->output->set_title($this->data['module']['title_pl']);
    }
    
    $this->data['table'] = [
      'columns' => [
        'name' => ['name' => 'name', 'title' => 'Name', 'width' => 'auto', 'filter' => ['type' => 'text']],
        'day_cycle' => ['name' => 'day_cycle', 'title' => 'Number Of Day', 'width' => 'auto', 'filter' => ['type' => 'text']],
        'tolerance' => ['name' => 'tolerance', 'title' => 'Tardiness Tolerance', 'width' => 'auto', 'filter' => ['type' => 'text']], 
        'action' => ['name' => 'action', 'title' => '', 'width' => '80px', 'filter' => ['type' => 'none']]
      ],
      'disable_sorting' => '0,4',
      'default_sort_col' => '1',
      'default_sort_order' => 'asc'
    ];
    $this->data['bulk_actions'] = [
      '' => lang('list_bulk_actions'),
      'delete' => lang('list_delete')
    ];

    $this->load->view('index', $this->data);
  }

  public function get_list()
  {
    $results = $this->work_patterns->get_list();

    foreach ($results['data'] as $key => $value) {
      $id = encode($value['id']);
      $name = '<a href="' . $this->data['module']['url'] . '/update/' . $id . '" class="btn-edit">' . $value['name'] . '</a>';
      $actions = '<a href="' . $this->data['module']['url'] . '/update/' . $id . '" class="btn btn-icon btn-primary btn-edit rounded-circle btn-sm" data-toggle="tooltip" data-original-title="Edit">
        <i class="feather icon-edit-2"></i></a>';
      $actions .= '<a href="' . $this->data['module']['url'] . '/delete/' . $id . '" class="btn btn-icon btn-danger btn-delete rounded-circle btn-sm" style="margin-left:5px" data-toggle="tooltip" data-original-title="Delete">
        <i class="feather icon-trash-2"></i></a>';
 
      $results['data'][$key]['check'] = '<div class="vs-checkbox-con vs-checkbox-primary"><input type="checkbox" name="data[]" value="' . $id . '" id="check' . $id . '">
        <span class="vs-checkbox vs-checkbox-sm"><span class="vs-checkbox--check"><i class="vs-icon feather icon-check"></i></span></span></div>';
      $results['data'][$key]['name'] = $name;
      
      $results['data'][$key]['action'] = $actions;
    }

    echo json_encode($results);
  }

  public function create()
  {
    $this->data['action'] = 'create';
    $this->data['title'] = 'Add New ' . $this->data['module']['title'];

    if (!$this->input->is_ajax_request()) {
      $this->output->set_template('default');
      $this->output->set_title($this->data['title']);
    }

    $this->load->view('form', $this->data);
  }

  public function update($id)
  {
    $this->data['action'] = 'update';
    $this->data['title'] = 'Edit ' . $this->data['module']['title'];
    $this->data['data'] = $this->get_data($id); 

    if (!$this->input->is_ajax_request()) {
      $this->output->set_template('default');
      $this->output->set_title($this->data['title']);
    }

    $this->load->view('form', $this->data);
  }
 
  public function save()
  {
    $this->input->is_ajax_request() or exit('No direct script access allowed!');
    
    $return = [
      'status' => 'success',
      'message' => ''
    ];

    $data = $this->input->post(null, true);
    $action = $data['action']; 
    unset($data['action']); 
  
    $this->form_validation->set_rules('name','name', 'required'); 

    if ($this->form_validation->run() === true) {

      if ($action == 'create') {
        $head['name'] = $data['name'];
        $head['day_cycle'] = $data['day_cycle'];
        $head['tolerance'] = $data['tolerance'];
        
        $save = $this->work_patterns->insert($head);
        if ($save) {
          do {  
            if (isset($data['items'])) {
                $item =[];
                $this->db->delete('work_pattern_detail',['id'=>$save]);
                foreach ($data['items'] as $row) { 
                    $item['id'] = $save; 
                    $item['name'] = $row['name']; 
                    $item['clock_in'] = $row['clock_in']; 
                    $item['clock_out'] = $row['clock_out'];  
                    
                    $this->db->insert('work_pattern_detail',$item);
                }
            }else{
                $return = ['message' => 'Item is Empty', 'status' => 'error'];
                break;
            } 
        } while (0);

          $return['status'] = 'success';
          $return['message'] = lang('insert_success_message'); 
        } else {
          $return['status'] = 'error';
          $return['message'] = lang('insert_error');
        }
      } else {
        $id = decode($data['id']);
        unset($data['id']);
        $save = $this->work_patterns->update($data, ['id' => $id]);
        if ($save) { 
          $return['status'] = 'success';
          $return['message'] = lang('update_success_message');
        } else {
          $return['status'] = 'error';
          $return['message'] = lang('update_error');
        }
      }
    } else {
      $return['status'] = 'error';
      $return['message'] = validation_errors();
    }

    echo json_encode($return);
  }

  public function get_val_work_pattern(){
    $id = $this->input->post('id');
    $value = $this->work_pattern_details->getvalue($id);
    echo json_encode($value);
  }

  public function status($id, $status)
  {
    $id = decode($id);
    $update = $this->work_patterns->update(['status' => $status], $id);
  }

  public function delete($id = null)
  {
    $delete = false;
    if (!is_null($id)) {
      $id = decode($id);
      $delete = $this->work_patterns->delete($id);
    } else {
      $data = $this->input->post('data', true);
      if ($data) {
        foreach ($data as $id) {
          $id = decode($id);
          $delete = ($id != 1) ? $this->work_patterns->delete($id) : false;
        }
      }
    }

    if ($delete) {
      echo 'success';
      $this->session->set_flashdata('success_message', successMessage(lang('delete_success_message')));
    } else {
      echo 'error';
      $this->session->set_flashdata('error_message', errorMessage(lang('delete_error_message')));
    }
  }

  public function get_data($id)
  { 
    $id = decode($id);
    $data = $this->work_patterns->get($id);
    if ($data) {
      return $data;
    } else {
      $this->session->set_flashdata('error_message', errorMessage(lang('not_found')));
      redirect($this->data['module']['url']);
    }
  }
 
}