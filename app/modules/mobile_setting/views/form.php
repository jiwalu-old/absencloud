<form id="form" action="<?php echo $module['url'].'/save'; ?>" method="post" enctype="multipart/form-data"  autocomplete="off">
  <div class="modal-header">
    <h5 class="modal-title" id="formModalTitle"><?php echo $title; ?></h5>
    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
  </div>
  <div class="modal-body">
    <div id="form-alert"><?php echo ($this->session->flashdata('message')) ? $this->session->flashdata('message') : ''; ?></div> 
    <input type="hidden" name="action" value="<?php echo $action; ?>">

    <?php if ($action == 'update') { ?>
      <input type="hidden" name="id" value="<?php echo encode($data->id); ?>"> 
    <?php } ?>
    
    <div class='row'>
 
        <div class='col-md-12'> 
            
            <div class="form-group required">
                <label>Work Pattern Name <span class="tx-danger">*</span></label>
                <input class="form-control" type="text" name="name" value="<?php echo ($action == 'update') ? $data->name : ''; ?>" required>
            </div>
             
            <div class="form-group required">
                <label>Number Of Days in Cycle <span class="tx-danger">*</span></label>
                <input class="form-control" type="text" id="day_cycle" name="day_cycle" value="<?php echo ($action == 'update') ? $data->day_cycle : ''; ?>" required>
            </div>
            <div class="form-group required">
                <label>Tardiness Tolerance <span class="tx-danger">*</span></label>
                <input class="form-control" type="text"  name="tolerance" value="<?php echo ($action == 'update') ? $data->tolerance : ''; ?>" required>
            </div>
           
            
            <div class="table-responsive">
              <table class="table mb-0">
                   
                  <tbody id="workday"> 
                  
                  </tbody>
              </table>
          </div>
             
             
             
            
               
        </div>
    </div>
  </div>
  <div class="modal-footer">
    <button type="submit" class="btn btn-primary waves-effect waves-light round">Save</button>
    <button type="button" class="btn btn-secondary waves-effect waves-light round" data-dismiss="modal">Cancel</button>
  </div>
</form>
  
<script type="text/javascript">
  
  $(function() {      
   
    $("#day_cycle").on("change", function(){
      $('.table-items').remove();
      var i;
      var x = $("#day_cycle").val();
      for (i = 0; i <x; ++i) {
          num = i+1;
          html = '<tr id="items-' + i + '" class="table-items">'+
                    '<td width="15%">Day '+num+'</td>'+
                    '<td width="35%"><select class="form-control" name="items['+i+'][name]"><option value="working day">Working Day</option><option value="day off">Day Off</option></select></td>'+
                    '<td width="25%"><input class="form-control" type="text" name="items['+i+'][clock_in]" ></td>'+
                    '<td width="25%"><input class="form-control" type="text" name="items['+i+'][clock_out]" ></td>'+
                  '</tr>'
          $('#workday').append(html);
      }
      
    });
    
    $('.dropify').dropify({
        messages: {
            default: 'Drag and drop to select image',
            replace: 'Replace',
            remove:  'Remove',
            error:   'error'
        }
    });
    
    $("#form").validate({
      errorElement: "span",
      errorClass: 'help-block',
      highlight: function (element) {
        $(element).parent().addClass('error');
      },
      unhighlight: function (element) {
        $(element).parent().removeClass('error');
      },
      submitHandler: function (form) {
        $('#loading').show();
        $(form).ajaxSubmit({
          success: function (response) {
            response = JSON.parse(response);
            if (response.status === 'success') {
              toastr.success(response.message, 'Success', {"closeButton": true});
              module.table.ajax.reload( null, false );
              $('#formModal').modal('hide');
            } else {
              toastr.error(response.message, 'Error', {"closeButton": true});
            }
            $('#loading').hide();
          },
          error: function (data) {
            $('#loading').hide();
          }
        });
      }
    });
  });

   
</script>