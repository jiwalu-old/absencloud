<form id="form" action="<?php echo $module['url'].'/save'; ?>" method="post" enctype="multipart/form-data"  autocomplete="off">
  <div class="modal-header">
    <h5 class="modal-title" id="formModalTitle"><?php echo $title; ?></h5>
    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
  </div>
  <div class="modal-body">
    <div id="form-alert"><?php echo ($this->session->flashdata('message')) ? $this->session->flashdata('message') : ''; ?></div> 
    
    <input type="hidden" name="id" value="<?php echo encode($data->id); ?>"> 
     
    <div class='row'>
 
        <div class='col-md-12'>
          
            <div class="form-group required">
                <label>Name <span class="tx-danger">*</span></label>
                <input class="form-control" type="text" name="name" value="<?php echo (isset($data->name)) ? $data->name : ''; ?>" readonly>
            </div>
            
            <div class="form-group required">
                <label>Work Pattern <span class="tx-danger">*</span></label>
                <?php echo form_dropdown('work_pattern', $work_pattern, ((isset($work->work_pattern)) ? $work->work_pattern : null), 'id="work_pattern" class="form-control select2" required'); ?>
            </div>

            <div class="form-group required">
                <label>Start Work <span class="tx-danger">*</span></label>
                <input class="form-control" type="text" name="valid" id="start_work" value="<?php echo (isset($work->valid)) ? $work->valid : ''; ?>" required>
            </div>

            <div class="form-group required">
                <label>Day <span class="tx-danger">*</span></label>
                <input class="form-control" type="number" name="day"  value="<?php echo (isset($work->day)) ? $work->day : ''; ?>" required>
            </div>

            <div class="table-responsive">
              <table class="table mb-0">
                  <tbody id="workday"> 
                  <thead class="thead-dark">
                      <tr>
                        <th scope="col">Day</th>
                        <th scope="col">Work Status</th>
                        <th scope="col">Clock In</th>
                        <th scope="col">Clock Out</th>
                      </tr>
                  </thead>
                  </tbody>
              </table>
            </div>
            
        </div>
    </div>
  </div>
  <div class="modal-footer">
    <button type="submit" class="btn btn-primary waves-effect waves-light round">Save</button>
    <button type="button" class="btn btn-secondary waves-effect waves-light round" data-dismiss="modal">Cancel</button>
  </div>
</form>
 
<?php $this->load->js("app-assets/vendors/js/pickers/pickadate/picker.date.js"); ?>
<?php $this->load->js("app-assets/vendors/js/pickers/pickadate/picker.js"); ?> 
<?php $this->load->css("app-assets/vendors/css/pickers/pickadate/pickadate.css"); ?>

<script type="text/javascript">
  
  $(function() {      
    <?php if(isset($work->work_pattern)){?>
      wp = "<?php echo $work->work_pattern; ?>";
      load_table(wp);
    <?php };?>
       
    
    $('#start_work').pickadate();
    $("#work_pattern").on("change", function(){
      wp = $("#work_pattern").val();
      load_table(wp);
    });
    
    $("#form").validate({
      errorElement: "span",
      errorClass: 'help-block',
      highlight: function (element) {
        $(element).parent().addClass('error');
      },
      unhighlight: function (element) {
        $(element).parent().removeClass('error');
      },
      submitHandler: function (form) {
        $('#loading').show();
        $(form).ajaxSubmit({
          success: function (response) {
            response = JSON.parse(response);
            if (response.status === 'success') {
              toastr.success(response.message, 'Success', {"closeButton": true});
              module.table.ajax.reload( null, false );
              $('#formModal').modal('hide');
            } else {
              toastr.error(response.message, 'Error', {"closeButton": true});
            }
            $('#loading').hide();
          },
          error: function (data) {
            $('#loading').hide();
          }
        });
      }
    });
  });

  function load_table(wp){
    $.ajax({
       type:'post',
       url:'<?php echo site_url('mobile_setting/work_pattern/get_val_work_pattern');?>',
       data : ({id:wp}),
       dataType : 'json',
       success : function(data){
          $('.table-items').remove();
          var i;
          for (i = 0; i < data.length; ++i) {
            console.log(data[i].name);
            num = i+1;
            html = '<tr id="items-' + i + '" class="table-items">'+
                        '<td width="15%">Day '+num+'</td>'+
                        '<td width="35%">'+data[i].name+'</td>'+
                        '<td width="25%">'+data[i].clock_in+'</td>'+
                        '<td width="25%">'+data[i].clock_out+'</td>'+
                      '</tr>'
              $('#workday').append(html);
          }
     
       }
     });
  }

   
</script>