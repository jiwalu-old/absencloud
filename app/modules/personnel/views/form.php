<form id="form" action="<?php echo $module['url'].'/save'; ?>" method="post" enctype="multipart/form-data"  autocomplete="off">
  <div class="modal-header">
    <h5 class="modal-title" id="formModalTitle"><?php echo $title; ?></h5>
    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
  </div>
  <div class="modal-body">
    <div id="form-alert"><?php echo ($this->session->flashdata('message')) ? $this->session->flashdata('message') : ''; ?></div> 
    <input type="hidden" name="action" value="<?php echo $action; ?>">
    <?php if ($action == 'update') { ?>
    <input type="hidden" name="id" value="<?php echo encode($data->id); ?>"> 
    <?php } ?>
    
    <div class='row'>
 
        <div class='col-md-12'>
            <div class="form-group required" align="center"> 
              <div style="width: 150px;">
                <input type="file" name="image" class="form-control dropify" data-height="150" data-width="150" <?php echo  ($action == 'update') ? 'data-default-file="'.base_url().'app-assets/images/personnel/'.$data->image.'"' : ''; ?> >
              </div>
               
            </div>
            
            <div class="form-group required">
                <label>Name <span class="tx-danger">*</span></label>
                <input class="form-control" type="text" name="name" value="<?php echo ($action == 'update') ? $data->name : ''; ?>" required>
            </div>
            <div class="form-group required">
                <label>Gender <span class="tx-danger">*</span></label>
                <?php echo form_dropdown('gender', $gender, (($action == 'update') ? $data->gender : null), 'class="form-control select2"  required'); ?>
            </div>
            <div class="form-group required">
                <label>Personnel ID <span class="tx-danger">*</span></label>
                <input class="form-control" type="text" name="personnel_id" value="<?php echo ($action == 'update') ? $data->personnel_id : ''; ?>" required>
            </div>
            <div class="form-group required">
                <label>Email <span class="tx-danger">*</span></label>
                <input class="form-control" type="text" name="email" value="<?php echo ($action == 'update') ? $data->email : ''; ?>" required>
            </div>
            <div class="form-group required">
                <label>Phone <span class="tx-danger">*</span></label>
                <input class="form-control" type="text" name="phone" value="<?php echo ($action == 'update') ? $data->phone : ''; ?>" required>
            </div>
            <div class="form-group required">
                <label>Start Work <span class="tx-danger">*</span></label>
                <input class="form-control" type="text" name="start_work" id="start_work" value="<?php echo ($action == 'update') ? $data->start_work : ''; ?>" required>
            </div>
            
               
        </div>
    </div>
  </div>
  <div class="modal-footer">
    <button type="submit" class="btn btn-primary waves-effect waves-light round">Save</button>
    <button type="button" class="btn btn-secondary waves-effect waves-light round" data-dismiss="modal">Cancel</button>
  </div>
</form>
 
<?php $this->load->js("app-assets/vendors/js/pickers/pickadate/picker.date.js"); ?>
<?php $this->load->js("app-assets/vendors/js/pickers/pickadate/picker.js"); ?> 
<?php $this->load->css("app-assets/vendors/css/pickers/pickadate/pickadate.css"); ?>

<script type="text/javascript">
  
  $(function() {      
    
    $('#start_work').pickadate();
    $('.dropify').dropify({
        messages: {
            default: 'Drag and drop to select image',
            replace: 'Replace',
            remove:  'Remove',
            error:   'error'
        }
    });
    
    $("#form").validate({
      errorElement: "span",
      errorClass: 'help-block',
      highlight: function (element) {
        $(element).parent().addClass('error');
      },
      unhighlight: function (element) {
        $(element).parent().removeClass('error');
      },
      submitHandler: function (form) {
        $('#loading').show();
        $(form).ajaxSubmit({
          success: function (response) {
            response = JSON.parse(response);
            if (response.status === 'success') {
              toastr.success(response.message, 'Success', {"closeButton": true});
              module.table.ajax.reload( null, false );
              $('#formModal').modal('hide');
            } else {
              toastr.error(response.message, 'Error', {"closeButton": true});
            }
            $('#loading').hide();
          },
          error: function (data) {
            $('#loading').hide();
          }
        });
      }
    });
  });

   
</script>