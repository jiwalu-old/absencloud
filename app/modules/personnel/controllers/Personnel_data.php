<?php defined('BASEPATH') or exit('No direct script access allowed!');

class Personnel_data extends App
{

  public function __construct()
  {
    parent::__construct();

    
    $this->load->library('upload');
    $this->load->model('personnel_datas'); 

    $this->data = [
      'module' => [
        'name' => 'personnel_data',
        'title' => 'Personnel Data',
        'title_pl' => 'Personnel Data',
        'url' => site_url('personnel/personnel_data')
      ],
      'menu' => ['menu' => 'personnel', 'submenu' => 'personnel_data'],
      'error_message' => $this->session->flashdata('error_message'),
      'success_message' => $this->session->flashdata('success_message'),
      'gender' => [
        '' => '',
        'male' => 'Male',
        'female' => 'Female'
      ]
    ];
  }

  public function index()
  {
    $this->privileged(['superadmin', 'admin']);
    if (!$this->input->is_ajax_request()) {
      $this->output->set_template('default');
      $this->output->set_title($this->data['module']['title_pl']);
    }
    
    $this->data['table'] = [
      'columns' => [
        'name' => ['name' => 'name', 'title' => 'Name', 'width' => 'auto', 'filter' => ['type' => 'text']],
        'gender' => ['name' => 'gender', 'title' => 'Gender', 'width' => 'auto', 'filter' => ['type' => 'text']],
        'personnel_id' => ['name' => 'personnel_id', 'title' => 'Personnel ID', 'width' => 'auto', 'filter' => ['type' => 'text']],
        'email' => ['name' => 'email', 'title' => 'Email', 'width' => 'auto', 'filter' => ['type' => 'text']],
        'phone' => ['name' => 'phone', 'title' => 'Phone', 'width' => '120px', 'filter' => ['type' => 'text']],
        'action' => ['name' => 'action', 'title' => '', 'width' => '80px', 'filter' => ['type' => 'none']]
      ],
      'disable_sorting' => '0,6',
      'default_sort_col' => '1',
      'default_sort_order' => 'asc'
    ];
    $this->data['bulk_actions'] = [
      '' => lang('list_bulk_actions'),
      'delete' => lang('list_delete')
    ];

    $this->load->view('index', $this->data);
  }

  public function get_list()
  {
    $results = $this->personnel_datas->get_list();

    foreach ($results['data'] as $key => $value) {
      $id = encode($value['id']);
      $name = '<a href="' . $this->data['module']['url'] . '/update/' . $id . '" class="btn-edit">' . $value['name'] . '</a>';
      $actions = '<a href="' . $this->data['module']['url'] . '/update/' . $id . '" class="btn btn-icon btn-primary btn-edit rounded-circle btn-sm" data-toggle="tooltip" data-original-title="Edit">
        <i class="feather icon-edit-2"></i></a>';
      $actions .= '<a href="' . $this->data['module']['url'] . '/delete/' . $id . '" class="btn btn-icon btn-danger btn-delete rounded-circle btn-sm" style="margin-left:5px" data-toggle="tooltip" data-original-title="Delete">
        <i class="feather icon-trash-2"></i></a>';
 
      $results['data'][$key]['check'] = '<div class="vs-checkbox-con vs-checkbox-primary"><input type="checkbox" name="data[]" value="' . $id . '" id="check' . $id . '">
        <span class="vs-checkbox vs-checkbox-sm"><span class="vs-checkbox--check"><i class="vs-icon feather icon-check"></i></span></span></div>';
      $results['data'][$key]['name'] = $name;
      
      $results['data'][$key]['action'] = $actions;
    }

    echo json_encode($results);
  }

  public function create()
  {
    $this->data['action'] = 'create';
    $this->data['title'] = 'Add New ' . $this->data['module']['title'];

    if (!$this->input->is_ajax_request()) {
      $this->output->set_template('default');
      $this->output->set_title($this->data['title']);
    }

    $this->load->view('form', $this->data);
  }

  public function update($id)
  {
    $this->data['action'] = 'update';
    $this->data['title'] = 'Edit ' . $this->data['module']['title'];
    $this->data['data'] = $this->get_data($id); 

    if (!$this->input->is_ajax_request()) {
      $this->output->set_template('default');
      $this->output->set_title($this->data['title']);
    }

    $this->load->view('form', $this->data);
  }
 
  public function save()
  {
    $this->input->is_ajax_request() or exit('No direct script access allowed!');
    $image='';
    $config['upload_path'] = './app-assets/images/personnel/';  
    $config['allowed_types'] = 'gif|jpg|png|jpeg|bmp';
    $config['encrypt_name'] = TRUE; 
    $this->upload->initialize($config);
    if(!empty($_FILES['image']['name']))
    {
        if ($this->upload->do_upload('image'))
          $gbr = $this->upload->data();
          $image=$gbr['file_name'];  
        
    }
    $return = [
      'status' => 'success',
      'message' => ''
    ];

    $data = $this->input->post(null, true);
    $action = $data['action']; 
    unset($data['action']);

    if($image!=''){ 
      $data['image'] = $image;
    }else{
      unset($data['image']); 
    }
     
    $this->form_validation->set_rules('name','name', 'required');
    $this->form_validation->set_rules('gender','gender', 'required');
    $this->form_validation->set_rules('personnel_id','personnel_id', 'required');
    $this->form_validation->set_rules('email','email', 'required');
    $this->form_validation->set_rules('phone','phone', 'required');
    $this->form_validation->set_rules('start_work','start_work', 'required');

    if ($this->form_validation->run() === true) {

      if ($action == 'create') {
         
        $save = $this->personnel_datas->insert($data);
        if ($save) {
          $return['status'] = 'success';
          $return['message'] = lang('insert_success_message'); 
        } else {
          $return['status'] = 'error';
          $return['message'] = lang('insert_error');
        }
      } else {
        $id = decode($data['id']);
        unset($data['id']);
        $save = $this->personnel_datas->update($data, ['id' => $id]);
        if ($save) { 
          $return['status'] = 'success';
          $return['message'] = lang('update_success_message');
        } else {
          $return['status'] = 'error';
          $return['message'] = lang('update_error');
        }
      }
    } else {
      $return['status'] = 'error';
      $return['message'] = validation_errors();
    }

    echo json_encode($return);
  }

  public function status($id, $status)
  {
    $id = decode($id);
    $update = $this->personnel_datas->update(['status' => $status], $id);
  }

  public function delete($id = null)
  {
    $delete = false;
    if (!is_null($id)) {
      $id = decode($id);
      $delete = $this->personnel_datas->delete($id);
    } else {
      $data = $this->input->post('data', true);
      if ($data) {
        foreach ($data as $id) {
          $id = decode($id);
          $delete = ($id != 1) ? $this->personnel_datas->delete($id) : false;
        }
      }
    }

    if ($delete) {
      echo 'success';
      $this->session->set_flashdata('success_message', successMessage(lang('delete_success_message')));
    } else {
      echo 'error';
      $this->session->set_flashdata('error_message', errorMessage(lang('delete_error_message')));
    }
  }

  public function get_data($id)
  { 
    $id = decode($id);
    $data = $this->personnel_datas->get($id);
    if ($data) {
      return $data;
    } else {
      $this->session->set_flashdata('error_message', errorMessage(lang('not_found')));
      redirect($this->data['module']['url']);
    }
  }
 
}