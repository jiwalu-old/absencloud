<?php defined('BASEPATH') or exit('No direct script access allowed!');

class Personnel_work extends App
{

  public function __construct()
  {
    parent::__construct();

     
    $this->load->model('personnel_works'); 
    $this->load->model('personnel_datas'); 
    $this->load->model('mobile_setting/work_pattern_details'); 

    $this->data = [
      'module' => [
        'name' => 'personnel_work',
        'title' => 'Personnel Work',
        'title_pl' => 'Personnel Work',
        'url' => site_url('personnel/personnel_work')
      ],
      'menu' => ['menu' => 'personnel', 'submenu' => 'personnel_work'],
      'error_message' => $this->session->flashdata('error_message'),
      'success_message' => $this->session->flashdata('success_message'), 
      'work_pattern' => $this->work_pattern_details->as_dropdown('name')->order_by('name','asc')->get_all(),
    ];
  }

  public function index()
  {
    $this->privileged(['superadmin', 'admin']);
    if (!$this->input->is_ajax_request()) {
      $this->output->set_template('default');
      $this->output->set_title($this->data['module']['title_pl']);
    }
    
    $this->data['table'] = [
      'columns' => [
        'name' => ['name' => 'name', 'title' => 'Name', 'width' => 'auto', 'filter' => ['type' => 'text']],
        'work_pattern' => ['name' => 'work_pattern', 'title' => 'Work Pattern', 'width' => 'auto', 'filter' => ['type' => 'text']],
        'valid' => ['name' => 'valid', 'title' => 'Start Date', 'width' => 'auto', 'filter' => ['type' => 'text']], 
        'action' => ['name' => 'action', 'title' => '', 'width' => '180px', 'filter' => ['type' => 'none']]
      ],
      'disable_sorting' => '0,4',
      'default_sort_col' => '1',
      'default_sort_order' => 'asc'
    ];
    $this->data['bulk_actions'] = [
      '' => lang('list_bulk_actions'),
      'delete' => lang('list_delete')
    ];

    $this->load->view('index_work', $this->data);
  }

  public function get_list()
  {
    $results = $this->personnel_works->get_list();

    foreach ($results['data'] as $key => $value) {
      $id = encode($value['id']); 
      if($value['work_pattern']!=null){
        $actions = '<a href="' . $this->data['module']['url'] . '/update/' . $id . '" class="btn btn-icon btn-primary btn-edit btn-md" data-toggle="tooltip" data-original-title="">
        Work Pattern History</a>'; 
      }else{
        $actions = '<a href="' . $this->data['module']['url'] . '/update/' . $id . '" class="btn btn-icon btn-secondary btn-edit btn-md" data-toggle="tooltip" data-original-title="">
        Add Work Pattern</a>';
      }
      
 
      $results['data'][$key]['check'] = '<div class="vs-checkbox-con vs-checkbox-primary"><input type="checkbox" name="data[]" value="' . $id . '" id="check' . $id . '">
        <span class="vs-checkbox vs-checkbox-sm"><span class="vs-checkbox--check"><i class="vs-icon feather icon-check"></i></span></span></div>';
      
      
      $results['data'][$key]['action'] = $actions;
    }

    echo json_encode($results);
  }
 
  public function update($id)
  { 
    $this->data['title'] = $this->data['module']['title'];
    $this->data['data'] = $this->get_data($id); 
    $this->data['work'] = $this->get_work($id);  

    if (!$this->input->is_ajax_request()) {
      $this->output->set_template('default');
      $this->output->set_title($this->data['title']);
    }

    $this->load->view('form_work', $this->data);
  }
 
  public function save()
  {
    $this->input->is_ajax_request() or exit('No direct script access allowed!');
     
    $return = [
      'status' => 'success',
      'message' => ''
    ];

    $data = $this->input->post(null, true);
    $check = $this->personnel_works->get(['personnel_id'=>decode($data['id'])]);
    
     
    $this->form_validation->set_rules('name','name', 'required'); 

    if ($this->form_validation->run() === true) {

      if($check) {         
        $id = decode($data['id']);
        unset($data['id']);
        $save = $this->personnel_works->update($data, ['personnel_id' => $id]);
        if ($save) { 
          $return['status'] = 'success';
          $return['message'] = lang('update_success_message');
        } else {
          $return['status'] = 'error';
          $return['message'] = lang('update_error');
        }
        
      } else {
        $data['personnel_id'] = decode($data['id']);
        unset($data['id']);
        $save = $this->personnel_works->insert($data);
        if ($save) {
          $return['status'] = 'success';
          $return['message'] = lang('insert_success_message'); 
        } else {
          $return['status'] = 'error';
          $return['message'] = lang('insert_error');
        }
      }
    } else {
      $return['status'] = 'error';
      $return['message'] = validation_errors();
    }

    echo json_encode($return);
  }
 
  public function get_data($id)
  { 
    $id = decode($id);
    $data = $this->personnel_datas->get($id);
    if ($data) {
      return $data;
    } else {
      $this->session->set_flashdata('error_message', errorMessage(lang('not_found')));
      redirect($this->data['module']['url']);
    }
  }
  public function get_work($id)
  { 
    $id = decode($id);
    $data = $this->personnel_works->get(['personnel_id'=>$id]);
    if ($data) {
      return $data;
    } else {
      return false;
    }
  }
 
}