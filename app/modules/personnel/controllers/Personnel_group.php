<?php defined('BASEPATH') or exit('No direct script access allowed!');

class Personnel_group extends App
{

  public function __construct()
  {
    parent::__construct();
 
    $this->load->model('personnel_groups'); 

    $this->data = [
      'module' => [
        'name' => 'personnel_group',
        'title' => 'Personnel Group',
        'title_pl' => 'Personnel Group',
        'url' => site_url('personnel/personnel_group')
      ],
      'menu' => ['menu' => 'personnel', 'submenu' => 'personnel_group'],
      'error_message' => $this->session->flashdata('error_message'),
      'success_message' => $this->session->flashdata('success_message'),
      
    ];
  }

  public function index()
  {
    $this->privileged(['superadmin', 'admin']);
    if (!$this->input->is_ajax_request()) {
      $this->output->set_template('default');
      $this->output->set_title($this->data['module']['title_pl']);
    }
    
    $this->data['table'] = [
      'columns' => [
        'name' => ['name' => 'name', 'title' => 'Name', 'width' => 'auto', 'filter' => ['type' => 'text']], 
        'action' => ['name' => 'action', 'title' => '', 'width' => '80px', 'filter' => ['type' => 'none']]
      ],
      'disable_sorting' => '0,2',
      'default_sort_col' => '1',
      'default_sort_order' => 'asc'
    ];
    $this->data['bulk_actions'] = [
      '' => lang('list_bulk_actions'),
      'delete' => lang('list_delete')
    ];

    $this->load->view('index', $this->data);
  }

  public function get_list()
  {
    $results = $this->personnel_groups->get_list();

    foreach ($results['data'] as $key => $value) {
      $id = encode($value['id']);
      $name = '<a href="' . $this->data['module']['url'] . '/update/' . $id . '" class="btn-edit">' . $value['name'] . '</a>';
      $actions = '<a href="' . $this->data['module']['url'] . '/update/' . $id . '" class="btn btn-icon btn-primary btn-edit rounded-circle btn-sm" data-toggle="tooltip" data-original-title="Edit">
        <i class="feather icon-edit-2"></i></a>';
      $actions .= '<a href="' . $this->data['module']['url'] . '/delete/' . $id . '" class="btn btn-icon btn-danger btn-delete rounded-circle btn-sm" style="margin-left:5px" data-toggle="tooltip" data-original-title="Delete">
        <i class="feather icon-trash-2"></i></a>';
 
      $results['data'][$key]['check'] = '<div class="vs-checkbox-con vs-checkbox-primary"><input type="checkbox" name="data[]" value="' . $id . '" id="check' . $id . '">
        <span class="vs-checkbox vs-checkbox-sm"><span class="vs-checkbox--check"><i class="vs-icon feather icon-check"></i></span></span></div>';
      $results['data'][$key]['name'] = $name;
      
      $results['data'][$key]['action'] = $actions;
    }

    echo json_encode($results);
  }

  public function create()
  {
    $this->data['action'] = 'create';
    $this->data['title'] = 'Add New ' . $this->data['module']['title'];

    if (!$this->input->is_ajax_request()) {
      $this->output->set_template('default');
      $this->output->set_title($this->data['title']);
    }

    $this->load->view('form_group', $this->data);
  }

  public function update($id)
  {
    $this->data['action'] = 'update';
    $this->data['title'] = 'Edit ' . $this->data['module']['title'];
    $this->data['data'] = $this->get_data($id); 

    if (!$this->input->is_ajax_request()) {
      $this->output->set_template('default');
      $this->output->set_title($this->data['title']);
    }

    $this->load->view('form_group', $this->data);
  }
 
  public function save()
  {
    $this->input->is_ajax_request() or exit('No direct script access allowed!');
    
    $return = [
      'status' => 'success',
      'message' => ''
    ];

    $data = $this->input->post(null, true);
    $action = $data['action']; 
    unset($data['action']);

    $this->form_validation->set_rules('name','name', 'required');

    if ($this->form_validation->run() === true) {

      if ($action == 'create') {
         
        $save = $this->personnel_groups->insert($data);
        if ($save) {
          $return['status'] = 'success';
          $return['message'] = lang('insert_success_message'); 
        } else {
          $return['status'] = 'error';
          $return['message'] = lang('insert_error');
        }
      } else {
        $id = decode($data['id']);
        unset($data['id']);
        $save = $this->personnel_groups->update($data, ['id' => $id]);
        if ($save) { 
          $return['status'] = 'success';
          $return['message'] = lang('update_success_message');
        } else {
          $return['status'] = 'error';
          $return['message'] = lang('update_error');
        }
      }
    } else {
      $return['status'] = 'error';
      $return['message'] = validation_errors();
    }

    echo json_encode($return);
  }

  public function status($id, $status)
  {
    $id = decode($id);
    $update = $this->personnel_groups->update(['status' => $status], $id);
  }

  public function delete($id = null)
  {
    $delete = false;
    if (!is_null($id)) {
      $id = decode($id);
      $delete = $this->personnel_groups->delete($id);
    } else {
      $data = $this->input->post('data', true);
      if ($data) {
        foreach ($data as $id) {
          $id = decode($id);
          $delete = ($id != 1) ? $this->personnel_groups->delete($id) : false;
        }
      }
    }

    if ($delete) {
      echo 'success';
      $this->session->set_flashdata('success_message', successMessage(lang('delete_success_message')));
    } else {
      echo 'error';
      $this->session->set_flashdata('error_message', errorMessage(lang('delete_error_message')));
    }
  }

  public function get_data($id)
  { 
    $id = decode($id);
    $data = $this->personnel_groups->get($id);
    if ($data) {
      return $data;
    } else {
      $this->session->set_flashdata('error_message', errorMessage(lang('not_found')));
      redirect($this->data['module']['url']);
    }
  }
 
}