<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Personnel_works extends MY_Model {

  public $table = 'work_time';
  public $primary_key = 'id';

  public function __construct()
  {
    parent::__construct();
    $this->soft_deletes = TRUE;
  } 

  public function get_list()
  {
    $this->load->library('datatable'); 
    $this->datatable->select("p.id,p.name,work_pattern,valid");
    $this->datatable->join("$this->table w","p.id=w.personnel_id","left");  
    $this->datatable->from("personnel p");  
    $this->datatable->where("p.deleted_at", null);
     
    
    return json_decode($this->datatable->generate(), true);
  }
 
  
}