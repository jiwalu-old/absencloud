<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Personnel_datas extends MY_Model {

  public $table = 'personnel';
  public $primary_key = 'id';

  public function __construct()
  {
    parent::__construct();
    $this->soft_deletes = TRUE;
  } 

  public function get_list()
  {
    $this->load->library('datatable'); 
    $this->datatable->select("*");
    $this->datatable->from("$this->table");  
    $this->datatable->where("deleted_at", null);
     
    
    return json_decode($this->datatable->generate(), true);
  }
  
}