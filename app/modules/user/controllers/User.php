<?php defined('BASEPATH') or exit('No direct script access allowed!');

class User extends App
{

  public function __construct()
  {
    parent::__construct();

    
    $this->load->library('upload');
    $this->load->model('users');
    $this->load->model('users_read');
    $this->load->model('user_group/users_group_read');

    $this->data = [
      'module' => [
        'name' => 'user',
        'title' => 'User',
        'title_pl' => 'Users',
        'url' => site_url('user')
      ],
      'menu' => ['menu' => 'administration', 'submenu' => 'user'],
      'error_message' => $this->session->flashdata('error_message'),
      'success_message' => $this->session->flashdata('success_message'),
      // 'customer_id'         => $this->users_group_read->as_dropdown('name')->get_all(),
      'status' => [
        '' => '- Select -',
        'active' => 'Active',
        'inactive' => 'Inactive'
      ]
    ];
  }

  public function index()
  {
    $this->privileged(['superadmin', 'admin']);
    if (!$this->input->is_ajax_request()) {
      $this->output->set_template('default');
      $this->output->set_title($this->data['module']['title_pl']);
    }

    $this->data['table'] = [
      'columns' => [
        'username' => ['name' => 'u.username', 'title' => 'Username', 'width' => 'auto', 'filter' => ['type' => 'text']],
        'name' => ['name' => 'u.name', 'title' => 'Full Name', 'width' => 'auto', 'filter' => ['type' => 'text']],
        'role' => ['name' => 'g.name', 'title' => 'User Type', 'width' => 'auto', 'filter' => ['type' => 'text']],
        'last_login' => ['name' => 'u.last_login', 'title' => 'Last Login', 'width' => 'auto', 'filter' => ['type' => 'text']],
        'status' => ['name' => 'u.status', 'title' => 'Status', 'width' => '120px', 'filter' => ['type' => 'dropdown', 'dropdown' => $this->data['status']]],
        'action' => ['name' => 'action', 'title' => '', 'width' => '80px', 'filter' => ['type' => 'none']]
      ],
      'disable_sorting' => '0,6',
      'default_sort_col' => '1',
      'default_sort_order' => 'asc'
    ];
    $this->data['bulk_actions'] = [
      '' => lang('list_bulk_actions'),
      'delete' => lang('list_delete')
    ];

    $this->load->view('index', $this->data);
  }

  public function get_list()
  {
    $results = $this->users_read->get_list();

    foreach ($results['data'] as $key => $value) {
      $id = encode($value['id']);
      $name = '<a href="' . $this->data['module']['url'] . '/update/' . $id . '" class="btn-edit">' . $value['username'] . '</a>';
      $actions = '<a href="' . $this->data['module']['url'] . '/update/' . $id . '" class="btn btn-icon btn-primary btn-edit rounded-circle btn-sm" data-toggle="tooltip" data-original-title="Edit">
        <i class="feather icon-edit-2"></i></a>';
      $actions .= '<a href="' . $this->data['module']['url'] . '/delete/' . $id . '" class="btn btn-icon btn-danger btn-delete rounded-circle btn-sm" style="margin-left:5px" data-toggle="tooltip" data-original-title="Delete">
        <i class="feather icon-trash-2"></i></a>';

      switch ($value['status']) {
        case 'active': $status = '<a class="btn btn-rounded btn-sm btn-ajax btn-outline-success px-1" href="'.$this->data['module']['url'].'/status/'.$id.'/inactive">Active</a>'; break;
        case 'inactive': $status = '<a class="btn btn-rounded btn-sm btn-ajax btn-outline-danger px-1" href="'.$this->data['module']['url'].'/status/'.$id.'/active">Inactive</a>'; break;
        default: $status = ''; break;
      }

      $results['data'][$key]['check'] = '<div class="vs-checkbox-con vs-checkbox-primary"><input type="checkbox" name="data[]" value="' . $id . '" id="check' . $id . '">
        <span class="vs-checkbox vs-checkbox-sm"><span class="vs-checkbox--check"><i class="vs-icon feather icon-check"></i></span></span></div>';
      $results['data'][$key]['username'] = $name;
      $results['data'][$key]['last_login'] = (!is_null($value['last_login'])) ? date_normal($value['last_login']) : '-';
      $results['data'][$key]['status'] = $status;
      $results['data'][$key]['action'] = $actions;
    }

    echo json_encode($results);
  }

  public function create()
  {
    $this->data['action'] = 'create';
    $this->data['title'] = 'Add New ' . $this->data['module']['title'];
    $this->data['groups'] = $this->users_group_read->as_dropdown('title')->order_by('id', 'desc')->get_all(['name !=' => 'superadmin']);

    if (!$this->input->is_ajax_request()) {
      $this->output->set_template('default');
      $this->output->set_title($this->data['title']);
    }

    $this->load->view('form', $this->data);
  }

  public function update($id)
  {
    $this->data['action'] = 'update';
    $this->data['title'] = 'Edit ' . $this->data['module']['title'];
    $this->data['data'] = $this->get_data($id);
    $this->data['groups'] = $this->users_group_read->as_dropdown('title')->order_by('id', 'desc')->get_all(['name !=' => 'superadmin','company_id' => userinfo('company_id')]);

    if (!$this->input->is_ajax_request()) {
      $this->output->set_template('default');
      $this->output->set_title($this->data['title']);
    }

    $this->load->view('form', $this->data);
  }

  public function profile()
  {
    $this->data['action'] = 'update';
    $this->data['title'] = 'Profile';
    $this->data['baseurl'] = base_url();
    $this->data['data'] = $this->get_data(encode(userinfo('id')));
    $this->data['is_profile'] = true;
        // $this->data['groups'] = $this->users_group_read->as_dropdown('name')->order_by('id','desc')->get_all();

    if (!$this->input->is_ajax_request()) {
      $this->output->set_template('default');
      $this->output->set_title($this->data['title']);
    }

    $this->load->view('profiles', $this->data);
  }

  public function save()
  {
    $this->input->is_ajax_request() or exit('No direct script access allowed!');
    $image='';
    $config['upload_path'] = './app-assets/images/user/';  
    $config['allowed_types'] = 'gif|jpg|png|jpeg|bmp';
    $config['encrypt_name'] = TRUE; 
    $this->upload->initialize($config);
    if(!empty($_FILES['image']['name']))
    {
        if ($this->upload->do_upload('image'))
          $gbr = $this->upload->data();
          $image=$gbr['file_name'];  
        
    }
    $return = [
      'status' => 'success',
      'message' => ''
    ];

    $data = $this->input->post(null, true);
    $action = $data['action'];
    // print_r($image); exit();
    unset($data['action']);

    $data['company_id'] = userinfo('company_id');

    if($image!=''){ 
      $data['image'] = $image;
    }else{
      unset($data['image']); 
    }
    

    if ($action == 'create') {
      $this->form_validation->set_rules('username', 'Username', 'required');
    }
    $this->form_validation->set_rules('name', 'Full Name', 'required');
    $this->form_validation->set_rules('email', 'Email', 'required');
    $this->form_validation->set_rules('status', 'Status', 'required');

    if ($this->form_validation->run() === true) {

      if ($this->check_email($data, $action)) {
        $return['status'] = 'error';
        $return['message'] = 'User with email <strong>'.$data['email'].'</strong> is already registered.';
        echo json_encode($return);
        exit();
      }

      if (!empty($data['password'])) {
        $this->load->library('password');
        $data['password'] = $this->password->hash_password($data['password']);
      } else {
        unset($data['password']);
      }

      if ($action == 'create') {
        if ($this->check_username($data['username'])) {
          $return['status'] = 'error';
          $return['message'] = 'User with username <strong>'.$data['username'].'</strong> is already registered.';
          echo json_encode($return);
          exit();
        }
        $save = $this->users->insert($data);
        if ($save) {
          $return['status'] = 'success';
          $return['message'] = lang('insert_success_message'); 
        } else {
          $return['status'] = 'error';
          $return['message'] = lang('insert_error');
        }
      } else {
        $id = decode($data['id']);
        unset($data['id']);
        $save = $this->users->update($data, ['id' => $id]);
        if ($save) { 
          if ($id == userinfo('id')) { 
            $this->session->set_userdata('login_user', [
              'id' => $id,
              'group_id' => userinfo('group_id'),
              'company_id' => userinfo('company_id'),
              'company_name' => userinfo('company_name'),
              'email' => $data['email'],
              'name' => $data['name'],
              'username' => userinfo('username'),
              'role' => userinfo('role'),
              'image' => $image
            ]); 
          }

          require_once(APPPATH.'third_party/vendor/autoload.php'); 
          $options = array(
            'cluster' => 'ap1',
            'useTLS' => true
          );
          $pusher = new Pusher\Pusher(
            '4c14965b64bb3e5ebdc9',
            'fa184649e50aee9bdd6f',
            '919243',
            $options
          );
         
          $data['value']['image'] = $image;
          $pusher->trigger('absen-channel', 'my-event', $data);

          $return['status'] = 'success';
          $return['message'] = lang('update_success_message');
        } else {
          $return['status'] = 'error';
          $return['message'] = lang('update_error');
        }
      }
    } else {
      $return['status'] = 'error';
      $return['message'] = validation_errors();
    }

    echo json_encode($return);
  }

  public function status($id, $status)
  {
    $id = decode($id);
    $update = $this->users->update(['status' => $status], $id);
  }

  public function delete($id = null)
  {
    $delete = false;
    if (!is_null($id)) {
      $id = decode($id);
      $delete = $this->users->delete($id);
    } else {
      $data = $this->input->post('data', true);
      if ($data) {
        foreach ($data as $id) {
          $id = decode($id);
          $delete = ($id != 1) ? $this->users->delete($id) : false;
        }
      }
    }

    if ($delete) {
      echo 'success';
      $this->session->set_flashdata('success_message', successMessage(lang('delete_success_message')));
    } else {
      echo 'error';
      $this->session->set_flashdata('error_message', errorMessage(lang('delete_error_message')));
    }
  }

  public function get_data($id)
  {
    // echo $id; exit();
    $id = decode($id);
    $data = $this->users_read->get($id);
    if ($data) {
      return $data;
    } else {
      $this->session->set_flashdata('error_message', errorMessage(lang('not_found')));
      redirect($this->data['module']['url']);
    }
  }

  public function export()
  {
    $this->load->library('excel');

    $results = $this->users_read->export();

    $this->excel->set_array($results);
    $this->excel->set_column(['username', 'name', 'role', 'last_login', 'status']);
    $this->excel->set_header(['Username', 'Full Name', 'User Type', 'Last Login', 'Status']);
    $this->excel->exportTo2003('Absen_ExportUsers_'.date('YmdHis'));
  }

  private function check_username($username)
  {
    $check = $this->users_read->get(['username' => $username, 'company_id' => userinfo('company_id'), 'deleted_at' => NULL]);
    return $check;
  }
  
  private function check_email($data, $action)
  {
    if ($action == 'create') {
      $check = $this->users_read->get(['email' => $data['email'], 'company_id' => userinfo('company_id'), 'deleted_at' => NULL]);
    } else {
      $user = $this->get_data($data['id']);
      if ($user->email == $data['email']) {
        return false;
      } else {
        $check = $this->users_read->get(['email' => $data['email'], 'company_id' => userinfo('company_id'), 'deleted_at' => NULL]);
      }
    }
    return $check;
  }
}