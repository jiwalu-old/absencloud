<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Users extends MY_Model {

  public $table = 'users';
  public $primary_key = 'id';

  public function __construct()
  {
    parent::__construct();
    $this->soft_deletes = TRUE;
  }
//function select move to users_read
  
}