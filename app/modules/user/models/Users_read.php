<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Users_read extends MY_Model {
  protected $_database_connection = 'serverdb';
  public $table = 'app.users';
  public $primary_key = 'id';

  public function __construct()
  {
    parent::__construct();
    $this->soft_deletes = false;
  }

  public function get_list()
  {
    $this->load->library('datatable');
    $this->datatable->set_database('serverdb');
    $this->datatable->select("u.id, u.name, u.username, u.status, u.last_login, g.title as role");
    $this->datatable->from("$this->table u");
    $this->datatable->join("app.user_groups g", "u.group_id = g.id");
    $this->datatable->where("u.company_id", userinfo('company_id'));
    $this->datatable->where("u.deleted_at", null);
    
    if (userinfo('role') != 'superadmin') {
      $this->datatable->where("g.name !=", 'superadmin');
    }
    
    return json_decode($this->datatable->generate(), true);
  }

  public function export()
  {
    $this->_database->select("u.id, u.name, u.username, u.status, u.last_login, g.title as role");
    $this->_database->from("$this->table u");
    $this->_database->join("app.user_groups g", "u.group_id = g.id");
    $this->_database->where("u.company_id", userinfo('company_id'));
    $this->_database->where("u.deleted_at", null);
    $this->_database->order_by("u.username");
    $query = $this->_database->get();

    return $query->result_array();
  }
}