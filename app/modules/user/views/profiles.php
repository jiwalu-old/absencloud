<form id="form" action="<?php echo $module['url'].'/save'; ?>" method="post" enctype="multipart/form-data">
  <!-- <div class="modal-header">
    
    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
  </div> -->
    <div class="card">
        <div class="card-header">
            <div class="form-row w-100">
                <div class="col-auto">
                <h5  ><?php echo $title; ?></h5>
                </div> 
            </div>
        </div>

        <div class="card-body">
            <div id="form-alert"><?php echo ($this->session->flashdata('message')) ? $this->session->flashdata('message') : ''; ?></div>
            <!-- <input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>"> -->
            <input type="hidden" name="action" value="<?php echo $action; ?>">
            <?php if ($action == 'update') { ?>
            <input type="hidden" name="id" value="<?php echo encode($data->id); ?>">
            <?php } ?>
            <div class="row">
            <div class="col-sm-6">
                <div class="form-group <?php echo ($action == 'update') ? 'disabled' : 'required'; ?>">
                <label>Username</label>
                <input class="form-control" type="text" name="username" value="<?php echo ($action == 'update') ? $data->username : ''; ?>" <?php echo ($action == 'update') ? 'disabled' : 'required'; ?>>
                </div>
                <div class="form-group required">
                <label>Full Name</label>
                <input class="form-control" type="text" name="name" value="<?php echo ($action == 'update') ? $data->name : ''; ?>" required>
                </div>
                <div class="form-group required">
                <label>Email</label>
                <input class="form-control" type="email" name="email" value="<?php echo ($action == 'update') ? $data->email : ''; ?>" required>
                </div>

                <div class="form-group <?php echo ($action == 'update') ? '' : 'required'; ?>">
                <label>Password <?php echo ($action == 'update') ? '<small>(Leave empty if not changed)</small>' : ''; ?></label>
                <input class="form-control" type="password" name="password" <?php echo ($action == 'update') ? '' : 'required'; ?>>
                </div>
                <?php if (!isset($is_profile)): ?>
                <div class="form-group required">
                <label>User Role</label>
                <?php
                $selected_role = ($action == 'update') ? $data->group_id : null;
                echo form_dropdown('group_id', $groups, $selected_role, 'class="form-control" required');
                ?>
                </div>
                <?php endif; ?>

                <div class="form-group required">
                <label>Status</label>
                <?php
                $status_id = ($action == 'update') ? $data->status : null;
                echo form_dropdown('status', $status, $status_id, 'class="form-control" required');
                ?>
                </div>

            </div>

            <div class="col-sm-6"> 
                <div class="form-group">
                <label>&nbsp;</label>
                <input type="file" name="image" class="form-control dropify" data-height="300" <?php echo ($action == 'update')?'data-default-file="'.$baseurl.'app-assets/images/user/'.$data->image.'"':''; ?> >
                </div> 
            </div>
            </div>
        </div>
        <div class="card-footer">
            <button type="submit" class="btn btn-primary waves-effect waves-light round">Save</button>
            <!-- <button type="button" class="btn btn-secondary round" data-dismiss="modal">Cancel</button> -->
        </div>
    </div>

</form>

<?php $this->load->css('app-assets/vendors/dropify/dist/css/dropify.min.css'); ?>
<?php $this->load->js('app-assets/vendors/dropify/dist/js/dropify.min.js'); ?>

<script type="text/javascript">
$(function() {
  $('.dropify').dropify({
            messages: {
                default: 'Drag and drop to select image',
                replace: 'Replace',
                remove:  'Remove',
                error:   'error'
            }
        });

  $("#form").validate({
    errorElement: "span",
    errorClass: 'help-block',
    highlight: function (element) {
      $(element).parent().addClass('error');
    },
    unhighlight: function (element) {
      $(element).parent().removeClass('error');
    },
    submitHandler: function (form) {
      $('#loading').show();
      $(form).ajaxSubmit({
        success: function (response) {
          response = JSON.parse(response);
          if (response.status === 'success') {
            toastr.success(response.message, 'Success', {"closeButton": true});
            module.table.ajax.reload( null, false );
            $('#formModal').modal('hide');
          } else {
            toastr.error(response.message, 'Error', {"closeButton": true});
          }
          $('#loading').hide();
        },
        error: function (data) {
          $('#loading').hide();
        }
      });
    }
  });
});
</script>