<div class="content-header row">
  <div class="content-header-left col-md-6 col-12 mb-2">
    <div class="row breadcrumbs-top">
      <div class="col-12">
        <h2 class="content-header-title float-left mb-0"><?php echo $module['title_pl']; ?></h2>
      </div>
    </div>
  </div>
  <div class="content-header-right text-md-right col-md-6 col-12 d-md-block d-none">
    <?php if (!isset($is_profile)): ?>
    <a href="<?php echo $module['url']; ?>" class="btn btn-warning ajax-load" title="<?php echo $module['title']; ?>">
      <i class="fa fa-angle-left"></i> Back
    </a>
    <?php endif; ?>
  </div>
</div>

<div class="br-pagebody">
    <div id="alert-container"><?php echo ($this->session->flashdata('message')) ? $this->session->flashdata('message') : ''; ?></div>
    <div class="br-section-wrapper pos-relative pd-20">
        <form id="form" action="<?php echo $module['url'].'/save'; ?>" method="post" enctype="multipart/form-data">
            <input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>">
            <input type="hidden" name="action" value="<?php echo $action; ?>">
            <input type="hidden" name="id" value="<?php echo ($action == 'update') ? encode($data->id) : ''; ?>">
            <div class="row">
                <div class="col-sm-6">
                    <div class="form-group form-group-default <?php echo ($action == 'update') ? 'disabled' : 'required'; ?>">
                        <label>Username</label>
                        <input class="form-control" type="text" name="username" value="<?php echo ($action == 'update') ? $data->username : ''; ?>" <?php echo ($action == 'update') ? 'disabled' : 'required'; ?>>
                    </div>
                    <div class="form-group form-group-default required">
                        <label>Full Name</label>
                        <input class="form-control" type="text" name="name" value="<?php echo ($action == 'update') ? $data->name : ''; ?>" required>
                    </div>
                    <div class="form-group form-group-default required">
                        <label>Email</label>
                        <input class="form-control" type="text" name="email" value="<?php echo ($action == 'update') ? $data->email : ''; ?>" required>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="form-group form-group-default <?php echo ($action == 'update') ? '' : 'required'; ?>">
                        <label>Password <?php echo ($action == 'update') ? '<small>(Leave empty if not changed)</small>' : ''; ?></label>
                        <input class="form-control" type="password" name="password" <?php echo ($action == 'update') ? '' : 'required'; ?>>
                    </div>
                    <?php if (!isset($is_profile)): ?>
                        <div class="form-group form-group-default form-group-default-select2 required">
                            <label>User Role</label>
                            <?php
                            $selected_role = ($action == 'update') ? $data->group_id : null;
                            echo form_dropdown('group_id', $groups, $selected_role, 'class="form-control" data-init-plugin="select2" data-disable-search="true" required'); ?>
                        </div>
                    <?php endif; ?>
                    <div class="form-group form-group-default form-group-default-select2 required">
                        <label>Status</label>
                        <?php
                        $status_id = ($action == 'update') ? $data->status : null;
                        echo form_dropdown('status', $status, $status_id, 'class="form-control" data-init-plugin="select2" data-disable-search="true" required'); ?>
                    </div>
                </div>
            </div>
            <button type="submit" class="btn btn-primary"><i class="icon ion-md-checkmark"></i> Save</button>
            <?php if (!isset($is_profile)): ?>
                <a href="<?php echo $module['url']; ?>" class="btn btn-danger ajax-load" title="<?php echo $module['title_pl']; ?>"><i class="icon ion-md-close"></i> Cancel</a>
            <?php endif; ?>
        </form>
        <div id="loading" class="bg-white-8 h-100 w-100 pos-absolute t-0 l-0" style="display: none;">
            <div class="d-flex h-100 w-100 pos-absolute align-items-center t-0 l-0">
                <div class="sk-cube-grid">
                    <div class="sk-cube sk-cube1 bg-info"></div>
                    <div class="sk-cube sk-cube2 bg-info"></div>
                    <div class="sk-cube sk-cube3 bg-info"></div>
                    <div class="sk-cube sk-cube4 bg-info"></div>
                    <div class="sk-cube sk-cube5 bg-info"></div>
                    <div class="sk-cube sk-cube6 bg-info"></div>
                    <div class="sk-cube sk-cube7 bg-info"></div>
                    <div class="sk-cube sk-cube8 bg-info"></div>
                    <div class="sk-cube sk-cube9 bg-info"></div>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
$(document).ready(function () {
    var base_url = '<?php echo base_url(); ?>';
    var module_url = '<?php echo $module['url']; ?>';
    $('[data-init-plugin="select2"]').each(function() {
        $(this).select2({
            minimumResultsForSearch: ($(this).attr('data-disable-search') == 'true' ? -1 : 1)
        });
    });
    $('#form').parsley().on('form:submit', function(){
        $('#loading').fadeIn();
        $('#form .btn-primary, #form .btn-success').prop('disabled', true);
        $('#form').ajaxSubmit({
            target: '#alert-container',
            success: function (data) {
                $('#loading').fadeOut();
                $('#form .btn-primary, #form .btn-success').prop('disabled', false);
            },
            error: function (data) {
                $('#loading').fadeOut();
                $('#form .btn-primary, #form .btn-success').prop('disabled', false);
            }
        });
        return false;
    });
});
</script>