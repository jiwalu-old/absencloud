<div class="content-header">
  <div class="content-header-left mb-2">
    <div class="breadcrumbs-top">
      <h2 class="content-header-title mb-0"><?php echo $title; ?></h2>
    </div>
  </div>
</div>

<div class="content-body"> 
    <div id="map-container" class="m-0"> 
        <div id="alert-container"><?php echo ($this->session->flashdata('message')) ? $this->session->flashdata('message') : ''; ?></div>
        <div class="br-section-wrapper pos-relative pd-20">
            <form id="form" action="<?php echo $module['url'].'/save'; ?>" method="post" enctype="multipart/form-data">
            
            <input type="hidden" name="action" value="<?php echo $action; ?>">
            <input type="hidden" name="id" value="<?php echo ($action == 'update') ? encode($data->id) : ''; ?>">
            <input type="hidden" name="area" id="area" value="<?php echo ($action == 'update') ? $data->area : ''; ?>">

            <div id="toolbar" class="">  
                <!-- <div id="map" class="mg-b-20" style="height: 400px; border-radius:10px;"></div>  -->
            </div> 
            <br>

            <div class="card">
                <div class="card-content">
                <div id="map" class="mg-b-20" style="height: 400px;"></div>
                    <div class="card-body">
                         
                        <div class="form-group row">
                            <label class="form-control-label col-md-3">Location Name</label>
                            <div class="col-md-9">
                                <input class="form-control" type="text" name="name" value="<?php echo ($action == 'update') ? $data->name : ''; ?>" required>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="form-control-label col-md-3">Location Code</label>
                            <div class="col-md-9">
                                <input class="form-control" type="text" name="code" value="<?php echo ($action == 'update') ? $data->code : ''; ?>">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="form-control-label col-md-3">Description</label>
                            <div class="col-md-9">
                                <textarea class="form-control" name="description"><?php echo ($action == 'update') ? $data->description : ''; ?></textarea>
                            </div>
                        </div>
                        <div align='center'>                         
                            <button type="submit" class="btn btn-primary"><i class="icon ion-md-checkmark"></i> Save</button>
                            <a href="<?php echo $module['url']; ?>" class="btn btn-danger ajax-load" title="<?php echo $module['title_pl']; ?>"><i class="icon ion-md-close"></i> Cancel</a>
                        </div>
                    </form>
                    <div id="loading" class="bg-white-8 h-100 w-100 pos-absolute t-0 l-0" style="display: none;">
                        <div class="d-flex h-100 w-100 pos-absolute align-items-center t-0 l-0">
                            <div class="sk-cube-grid">
                                <div class="sk-cube sk-cube1 bg-info"></div>
                                <div class="sk-cube sk-cube2 bg-info"></div>
                                <div class="sk-cube sk-cube3 bg-info"></div>
                                <div class="sk-cube sk-cube4 bg-info"></div>
                                <div class="sk-cube sk-cube5 bg-info"></div>
                                <div class="sk-cube sk-cube6 bg-info"></div>
                                <div class="sk-cube sk-cube7 bg-info"></div>
                                <div class="sk-cube sk-cube8 bg-info"></div>
                                <div class="sk-cube sk-cube9 bg-info"></div>
                            </div>
                        </div> 
                    </div>
                </div>
            </div>
            
        </div>
    </div>
</div>

<style type="text/css">
    .select-map {
        position: absolute;
        z-index: 1000;
        top: 50%;
        transform: translateY(-139%);
        padding-top: 57px;
        width: 150px;
        padding-left: 38px;
    }
</style>

<link rel="stylesheet" href="<?php echo base_url(); ?>app-assets/lib/leaflet/leaflet.css">
<link rel="stylesheet" href="<?php echo base_url(); ?>app-assets/lib/leaflet/plugins/draw/L.draw.css">
<link rel="stylesheet" href="<?php echo base_url(); ?>app-assets/lib/leaflet/plugins/google-places-autocomplete/L.gplaces-autocomplete.css">
<script src="https://maps.googleapis.com/maps/api/js?key=<?php echo MAPS_APIKEY; ?>&libraries=places" async defer></script>
<script src="<?php echo base_url(); ?>app-assets/lib/leaflet/leaflet.js"></script>
<script src="<?php echo base_url(); ?>app-assets/lib/parsleyjs/parsley.min.js"></script>
<script src="<?php echo base_url(); ?>app-assets/lib/leaflet/plugins/draw/L.draw.js"></script>
<script src="<?php echo base_url(); ?>app-assets/lib/leaflet/plugins/google-mutant/L.GoogleMutant.js"></script>
<script src="<?php echo base_url(); ?>app-assets/lib/leaflet/plugins/control-custom/L.Control.Custom.js"></script>
<script src="<?php echo base_url(); ?>app-assets/lib/leaflet/plugins/google-places-autocomplete/L.gplaces-autocomplete.js"></script>
<script type="text/javascript">
var base_url = '<?php echo base_url(); ?>';
var module_url = '<?php echo $module['url']; ?>';
 
var map = null;

$(document).ready(function () {  
    $('#form').parsley().on('form:submit', function(){
        $('#loading').fadeIn();
        $('#form .btn-primary, #form .btn-success').prop('disabled', true);
        $('#form').ajaxSubmit({
            target: '#alert-container',
            success: function (data) {
                $('#loading').fadeOut();
                $('#form .btn-primary, #form .btn-success').prop('disabled', false);
            },
            error: function (data) {
                $('#loading').fadeOut();
                $('#form .btn-primary, #form .btn-success').prop('disabled', false);
            }
        });
        return false;
    });
    initMap();
    $('#selectMap').change(function () {
      pilih($(this).val()); 
  });
});

function pilih(val){ 
  L.gridLayer.googleMutant({
        maxZoom: 24,
        type: val
    }).addTo(map);
}

function initMap() {
    var southWest = L.latLng(-11, 95),
        northEast = L.latLng(6, 141),
        indonesia = L.latLngBounds(southWest, northEast);
     
    map = new L.Map('map', {
        center: indonesia.getCenter(),
        zoom: 5,
        zoomControl: false,
        attributionControl: false,
        editable: true,
        editOptions: {

        },
    });
 
    pilih('roadmap');
    var pickmap = L.control.custom({
        position: 'topleft',
        content: '<div class="select-map"><select id="selectMap" class="form-control"><option value="roadmap">Road Map</option>  <option value="satellite">Satellite</option> <option value="hybrid">Hybrid</option> </select></div>'
    }); 

    L.control.zoom().addTo(map);
    
    pickmap.addTo(map);
    L.control.scale().addTo(map);

    <?php if ($action == 'update') { ?>
      var polygonLatLngs = <?php echo $data->area; ?>;
      var polygon = new L.Polygon(polygonLatLngs, {
          editable: true
      }).addTo(map);
      map.fitBounds(polygon.getBounds());
      polygon.on('edit', function() {
          $('#area').val(polygon.getLatLngs());
      });
    <?php } else { ?>
      var drawnItems = new L.FeatureGroup();
      map.addLayer(drawnItems);

      map.addControl(new L.Control.Draw({
        edit: {
            featureGroup: drawnItems,
            poly : {
                allowIntersection : false
            }
        },
        draw: {
            circle: false,
            circlemarker: false,
            rectangle: false,
            polyline: false,
            marker: false,
            polygon : {
                allowIntersection: false,
                showArea:true
            }
        }
    }));
 
    var _round = function(num, len) {
        return Math.round(num*(Math.pow(10, len)))/(Math.pow(10, len));
    };
    
    var strLatLng = function(latlng) {
        return "("+_round(latlng.lat, 6)+", "+_round(latlng.lng, 6)+")";
    };
 
    var getPopupContent = function(layer) { 
        if (layer instanceof L.Marker || layer instanceof L.CircleMarker) {
            return strLatLng(layer.getLatLng());
        // Circle - lat/long, radius
        } else if (layer instanceof L.Circle) {
            var center = layer.getLatLng(),
                radius = layer.getRadius();
            return "Center: "+strLatLng(center)+"<br />"
                  +"Radius: "+_round(radius, 2)+" m";
        // Rectangle/Polygon - area
        } else if (layer instanceof L.Polygon) {
            var latlngs = layer._defaultShape ? layer._defaultShape() : layer.getLatLngs(),
                area = L.GeometryUtil.geodesicArea(latlngs);
            return "Area: "+L.GeometryUtil.readableArea(area, true);
        // Polyline - distance
        } else if (layer instanceof L.Polyline) {
            var latlngs = layer._defaultShape ? layer._defaultShape() : layer.getLatLngs(),
                distance = 0;
            if (latlngs.length < 2) {
                return "Distance: N/A";
            } else {
                for (var i = 0; i < latlngs.length-1; i++) {
                    distance += latlngs[i].distanceTo(latlngs[i+1]);
                }
                return "Distance: "+_round(distance, 2)+" m";
            }
        }
        return null;
    };

    map.on(L.Draw.Event.CREATED, function(event) {
        var layer = event.layer;
        var content = getPopupContent(layer);
        if (content !== null) {
            layer.bindPopup(content);
        }

        drawnItems.addLayer(layer);
        $('#area').val(layer.getLatLngs());
    });

    map.on(L.Draw.Event.EDITED, function(event) {
        var layers = event.layers,
            content = null;
        layers.eachLayer(function(layer) {
            content = getPopupContent(layer);
            if (content !== null) {
                layer.setPopupContent(content);
            }
            $('#area').val(layer.getLatLngs());
        });
    });
    <?php } ?>

    new L.Control.GPlaceAutocomplete({
        callback: function(place){
            var loc = place.geometry.location;
            map.setView( [loc.lat(), loc.lng()], 18);
        }
    }).addTo(map);
     
}


</script>