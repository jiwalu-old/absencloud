 
<div class="content-header">
  <div class="content-header-left mb-2">
    <div class="breadcrumbs-top">
      <h2 class="content-header-title mb-0"><?php echo $module['title_pl']; ?></h2>
    </div>
  </div>
</div>

<div class="content-body">
    <div class="card">
        <div class="card-header">
            <div class="form-row w-100">
                <div class="col-auto">
                    <a href="<?php echo $module['url']; ?>/create" class="btn btn-primary" title="Add New <?php echo $module['title']; ?>">
                    Add New
                    </a>
                </div>
                <div class="col-auto ml-auto btn-actions">
                    <input type="hidden" name="bulk_action" value="delete" id="bulk-action-select">
                    <button type="button" id="bulk-action-apply" class="btn btn-icon btn-danger rounded-circle" data-toggle="tooltip" data-original-title="Delete selected items" disabled><i class="feather icon-trash-2"></i></button>
    
                    <a href="<?php echo $module['url']; ?>/export" class="btn btn-icon btn-primary rounded-circle" data-toggle="tooltip" data-original-title="Download"><i class="feather icon-download"></i></a>
                </div>
            </div>
        </div>
        <div class="card-content">
            <div class="card-body">
                <table id="table-grid" class="table table-condensed table-striped table-hover">
                    <thead>
                        <tr>
                            <th style="width:20px">
                                <div class="vs-checkbox-con vs-checkbox-primary">
                                    <input type="checkbox" name="checkAll" value="" id="checkAll">
                                    <span class="vs-checkbox vs-checkbox-sm">
                                        <span class="vs-checkbox--check">
                                            <i class="vs-icon feather icon-check"></i>
                                        </span>
                                    </span>
                                </div>
                            </th>
                            <?php
                            if ($table['columns']) {
                                foreach ($table['columns'] as $key => $column) {
                                    echo '<th style="width:'.$column['width'].'">'.$column['title'].'</th>';
                                }
                            }
                            ?>
                        </tr>
                    </thead>
                    <tfoot>
                        <tr>
                            <th></th>
                            <?php if ($table['columns']): foreach ($table['columns'] as $key => $column): ?>
                            <th>
                                <?php
                                if ($column['filter']['type'] == 'text') {
                                    echo '<input type="text" class="form-control" placeholder="Search">';
                                } elseif ($column['filter']['type'] == 'date') {
                                    echo '<input type="text" class="form-control fc-datepicker" placeholder="Search">';
                                } elseif ($column['filter']['type'] == 'dropdown') {
                                    $dropdown_class = (isset($column['filter']['class'])) ? 'form-control '.$column['filter']['class'] : 'form-control';
                                    echo form_dropdown('filter', $column['filter']['dropdown'], NULL, 'class="'.$dropdown_class.'"');
                                }
                                ?>
                            </th>
                            <?php endforeach; endif; ?>
                        </tr>
                    </tfoot>
                </table>
                <div id="loading" class="bg-white-8 h-100 w-100 pos-absolute t-0 l-0" style="display: none;">
                    <div class="d-flex h-100 w-100 pos-absolute align-items-center t-0 l-0">
                        <div class="sk-cube-grid">
                            <div class="sk-cube sk-cube1 bg-info"></div>
                            <div class="sk-cube sk-cube2 bg-info"></div>
                            <div class="sk-cube sk-cube3 bg-info"></div>
                            <div class="sk-cube sk-cube4 bg-info"></div>
                            <div class="sk-cube sk-cube5 bg-info"></div>
                            <div class="sk-cube sk-cube6 bg-info"></div>
                            <div class="sk-cube sk-cube7 bg-info"></div>
                            <div class="sk-cube sk-cube8 bg-info"></div>
                            <div class="sk-cube sk-cube9 bg-info"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade show" id="formModal" tabindex="-1" role="dialog" aria-labelledby="formModalTitle" aria-modal="true">
  <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable" role="document">
    <div class="modal-content">
    </div>
  </div>
</div>
<div id="loading" style="display:none">
  <div class="spinner-border text-primary" role="status">
    <span class="sr-only">Loading...</span>
  </div>
</div>

<?php
$columns = '{"data":"check"},';
if ($table['columns']) {
    foreach ($table['columns'] as $key => $column) {
        $columns .= '{"data":"'.$key.'", "name":"'.$column['name'].'"},';
    }
}
?>
<script type="text/javascript">
var current_url = '<?php echo current_url(); ?>';
var module = {
    url: "<?php echo $module['url']; ?>",
    hash: "<?php echo $this->security->get_csrf_hash(); ?>"
};
 $(document).ready(function () {
    // $('#loading').fadeIn();
    module.table = $('#table-grid').on( 'processing.dt', function ( e, settings, processing ) {
        if (processing === true) {
            $('#loading').fadeIn();
        } else {
            $('#loading').fadeOut();
        }
    }).DataTable({
        "ajax": current_url + "/get_list",
        "columns": [<?php echo $columns; ?>],
        "columnDefs": [ { orderable: false, targets: [<?php echo $table['disable_sorting']; ?>] } ],
        "order": [[ <?php echo $table['default_sort_col'].', "'.$table['default_sort_order'].'"'; ?> ]]
    });
    module.table.columns().every( function() {
        var that = this;
        var search = $.fn.dataTable.util.throttle(
            function (val) {
                that.search(val).draw();
            }, 1000
        );
        $( 'input', this.footer() ).on( 'keyup', function (e) {
            if(e.keyCode == 13) {
                search(this.value);
            }
        });
        $( 'input', this.footer() ).on( 'change', function () {
            that.search( this.value ).draw();
        });
        $( 'select', this.footer() ).on( 'change', function () {
            that.search( this.value ).draw();
        });
    });

    // $('.select2, .dataTables_length select').select2({minimumResultsForSearch:-1});

    // ajax button
    $('body').on('click', '.btn-ajax', function(e){
        e.preventDefault();
        var url = $(this).attr('href');
        $.ajax({
            url: url,
            method: 'post',
            success: function(response){
                module.table.ajax.reload(null, false);
            }
        });
    });

    $('body').on('change', '#checkAll, input[name="data[]"]', function(){
        if ($('input[name="data[]"]:checked').length > 0) {
            $('#bulk-action').show();
        } else {
            $('#bulk-action').hide();
        }
    });
});
</script>