<?php defined('BASEPATH') or exit('No direct script access allowed!');

class Attendance_spot extends App
{

    public function __construct()
    {
        parent::__construct();

        $this->privileged(['superadmin','admin']);

        $this->load->model('attendance_spots'); 
        $this->load->library('api');

        $this->data = [
            'module' => [
                'name'     => 'attendance_spot',
                'title'    => 'Attendance Spot',
                'title_pl' => 'Attendance Spots',
                'url'      => site_url('attendance_spot')
            ],
            'menu'            => ['menu' => 'administration', 'submenu' => ''],
            'error_message'   => $this->session->flashdata('error_message'),
            'success_message' => $this->session->flashdata('success_message')
        ];
    }

    public function index()
    {
        if (!$this->input->is_ajax_request()) {
            $this->output->set_template('default');
            $this->output->set_title($this->data['module']['title_pl']);
        }

        $this->data['table'] = [
            'columns' => [
                'name' => ['name'  => 'name', 'title' => 'Location Name', 'width' => 'auto', 'filter' => ['type' => 'text']],
                'code' => ['name'  => 'code', 'title' => 'Location Code', 'width' => 'auto', 'filter' => ['type' => 'text']],
                'description' => ['name'  => 'description', 'title' => 'Description', 'width' => 'auto', 'filter' => ['type' => 'text']],
                'action' => ['name'  => 'action', 'title' => '', 'width' => '80px', 'filter' => ['type' => 'none']]
            ],
            'disable_sorting' => '0,4',
            'default_sort_col' => '1',
            'default_sort_order' => 'asc'
        ];

        $this->load->view('index', $this->data);
    }

    public function get_list()
    {
        $results = $this->attendance_spots->get_list();

        foreach ($results['data'] as $key => $value) {
            
            $id = encode($value['id']);
            $name = '<a href="'.$this->data['module']['url'].'/update/'.$id.'" class="" title="Edit '.$this->data['module']['title'].'">'.$value['name'].'</a>';

            $actions = '<a href="'.$this->data['module']['url'].'/update/'.$id.'" class="btn btn-icon btn-primary rounded-circle btn-sm" data-toggle="tooltip" data-original-title="Edit" title="Edit '.$this->data['module']['title'].'">
                <i class="feather icon-edit-2"></i></a>';
                
                $actions .= '<a href="' . $this->data['module']['url'] . '/delete/' . $id . '" class="btn btn-icon btn-danger btn-delete rounded-circle btn-sm" style="margin-left:5px" data-toggle="tooltip" data-original-title="Delete">
                <i class="feather icon-trash-2"></i></a>';

            $results['data'][$key]['check'] = '<div class="vs-checkbox-con vs-checkbox-primary"><input type="checkbox" name="data[]" value="' . $id . '" id="check' . $id . '">
                <span class="vs-checkbox vs-checkbox-sm"><span class="vs-checkbox--check"><i class="vs-icon feather icon-check"></i></span></span></div>';
            $results['data'][$key]['name'] = $name;
            $results['data'][$key]['action'] = $actions;
        }

        echo json_encode($results);
    }

    public function create()
    {
        $this->data['action'] = 'create';
        $this->data['title'] = 'Add New '.$this->data['module']['title'];

        if (!$this->input->is_ajax_request()) {
            $this->output->set_template('default');
            $this->output->set_title($this->data['title']);
        }

        $this->load->view('form', $this->data);
    }

    public function update($id)
    {
        $this->data['action'] = 'update';
        $this->data['title'] = 'Edit '.$this->data['module']['title'];
        $this->data['data'] = $this->get_data($id);

        if (!$this->input->is_ajax_request()) {
            $this->output->set_template('default');
            $this->output->set_title($this->data['title']);
        }

        $this->load->view('form', $this->data);
    }

    public function save(){
        $this->input->is_ajax_request() or exit('No direct script access allowed!');
        $return = [
            'status' => 'success',
            'message' => ''
        ];

        $this->form_validation->set_error_delimiters('<div class="error">', '</div>');
        $this->form_validation->set_rules('name', 'Location Name', 'required');
        $this->form_validation->set_rules('area', 'Location Area', 'required', ['required' => 'Please draw the location area on the maps']);

        if ($this->form_validation->run() === true) {
            $data = $this->input->post();
            $action = $data['action'];
            unset($data['action']);
 
            $data['type'] = 'polygon';

            if (substr($data['area'], 0,2) != '[[') {
                $data['area'] = str_replace('LatLng(', '[', $data['area']);
                $data['area'] = str_replace(')', ']', $data['area']);
                $data['area'] = '['.$data['area'].']';
            }

            $area = str_replace('[[', 'POLYGON((', $data['area']);
            $area = str_replace(']]', '))', $area);
            $area = str_replace('],[', ',', $area);
            $area = str_replace(', ', ' ', $area);

            if ($action == 'create') { 
                unset($data['id']);
 
                $save = $this->attendance_spots->insert($data);
                if ($save) { 
                    $this->session->set_flashdata('message', successMessage('Data has been successfully saved'));
                    echo ajaxRedirect($this->data['module']['name'], 0);
                } else {
                    echo errorMessage('Failed to save data, please try again.');
                }
            } else {
                $id = decode($data['id']);
                unset($data['id']);
                $save = $this->attendance_spots->update($data, $id);
                if ($save) { 
                    $this->session->set_flashdata('message', successMessage('Data has been successfully saved'));
                    echo ajaxRedirect($this->data['module']['name'], 0);
                } else {
                    echo errorMessage('Failed to save data, please try again.');
                }
            }
        } else {
            echo errorMessage(validation_errors());
        }

        echo json_encode($return); 
    }

    public function status($id, $status)
    {
        $id = decode($id);
        $update = $this->attendance_spots->update(['status' => $status], $id);
    }

    public function delete($id = NULL)
    {
        $delete = false;
        if (!is_null($id)) {
            $id = decode($id);
            $data = $this->attendance_spots->get($id);
            $delete = $this->attendance_spots->delete($id); 
        } else {
            $ids = $this->input->post('data', true);
            if ($ids) {
                foreach ($ids as $id) {
                    $id = decode($id);
                    $data = $this->attendance_spots->get($id);
                    $delete = $this->attendance_spots->delete($id); 
                }
            }
        }

        if ($delete) {
            echo 'success';
            $this->session->set_flashdata('success_message', successMessage(lang('delete_success_message')));
        } else {
            echo 'error';
            $this->session->set_flashdata('error_message', errorMessage(lang('delete_error_message')));
        }
    }

    public function get_data($id)
    {
        $id = decode($id);
        $data = $this->attendance_spots->get($id);
        if ($data) {
            return $data;
        } else {
            $this->session->set_flashdata('message', errorMessage(lang('not_found')));
            redirect($this->data['module']['url']);
        }
    }

    public function export()
    {
        $this->load->library('excel');

        $results = $this->attendance_spots->export();

        $this->excel->set_array($results);
        $this->excel->set_column(['name', 'description']);
        $this->excel->set_header(['Location Name', 'Description']);
        $this->excel->exportTo2003('Attendance');
    }
}
