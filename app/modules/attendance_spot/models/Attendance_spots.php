<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Attendance_spots extends MY_Model { 
    public $table = 'attendance_spots';
    public $primary_key = 'id';

    public function __construct()
    {
        parent::__construct();
        $this->soft_deletes = FALSE;
    }

    public function get_list()
    {
        $this->load->library('datatable'); 
        $this->datatable->select("*");
        $this->datatable->from($this->table); 

        return json_decode($this->datatable->generate(), true);
    }

    public function export()
    {
        $this->_database->select("name, description");
        $this->_database->from($this->table);
        $this->_database->where("company_id", userinfo('company_id'));
        $this->_database->order_by("name");
        $query = $this->_database->get();

        return $query->result_array();
    }
}
