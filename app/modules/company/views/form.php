<form id="form" action="<?php echo $module['url'].'/save'; ?>" method="post" enctype="multipart/form-data"  autocomplete="off">
  <div class="modal-header">
    <h5 class="modal-title" id="formModalTitle"><?php echo $title; ?></h5>
    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
  </div>
  <div class="modal-body">
    <div id="form-alert"><?php echo ($this->session->flashdata('message')) ? $this->session->flashdata('message') : ''; ?></div> 
    <input type="hidden" name="action" value="<?php echo $action; ?>">
    <?php if ($action == 'update') { ?>
    <input type="hidden" name="id" value="<?php echo encode($data->id); ?>"> 
    <?php } ?>
    
    <div class='row'>
         

        <div class='col-md-12'> 
            <div class="form-group required">
                <label>Username <span class="tx-danger">*</span></label>
                <input class="form-control" type="text" name="username" value="<?php echo ($action == 'update') ? $data->username : ''; ?>" required>
            </div>
            <div class="form-group required">
                <label>Fullname <span class="tx-danger">*</span></label>
                <input class="form-control" type="text" name="name" value="<?php echo ($action == 'update') ? $data->name : ''; ?>" required>
            </div>
            <div class="form-group required">
                <label>Email <span class="tx-danger">*</span></label>
                <input class="form-control" type="text" name="email" value="<?php echo ($action == 'update') ? $data->email : ''; ?>" required>
            </div>
            <div class="form-group required">
                <label>Password <?php echo ($action == 'update') ? '<small>(Leave empty if not changed)</small>' : '*'; ?></label>
                <input class="form-control" type="password" name="password" value="" <?php echo ($action == 'update') ?'' : 'required'; ?>>
            </div>
            <div class="form-group required">
                <label>User Role <span class="tx-danger">*</span></label>
                <?php
                $selected_role = ($action == 'update') ? $data->group_id : null;
                echo form_dropdown('group_id', $groups, $selected_role, 'class="form-control" required');
                ?>
            </div>
            <div class="form-group required">
                <label>Status <span class="tx-danger">*</span></label>
                <?php echo form_dropdown('status', $status, (($action == 'update') ? $data->status : null), 'class="form-control select2"  required'); ?>
            </div>
            
        
               
        </div>
    </div>
  </div>
  <div class="modal-footer">
    <button type="submit" class="btn btn-primary waves-effect waves-light round">Save</button>
    <button type="button" class="btn btn-secondary waves-effect waves-light round" data-dismiss="modal">Cancel</button>
  </div>
</form>
 

<script type="text/javascript">
  
  $(function() { 
     

    $("#form").validate({
      errorElement: "span",
      errorClass: 'help-block',
      highlight: function (element) {
        $(element).parent().addClass('error');
      },
      unhighlight: function (element) {
        $(element).parent().removeClass('error');
      },
      submitHandler: function (form) {
        $('#loading').show();
        $(form).ajaxSubmit({
          success: function (response) {
            response = JSON.parse(response);
            if (response.status === 'success') {
              toastr.success(response.message, 'Success', {"closeButton": true});
              module.table.ajax.reload( null, false );
              $('#formModal').modal('hide');
            } else {
              toastr.error(response.message, 'Error', {"closeButton": true});
            }
            $('#loading').hide();
          },
          error: function (data) {
            $('#loading').hide();
          }
        });
      }
    });
  });

   
</script>