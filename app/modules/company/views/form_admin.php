<form id="form" action="<?php echo $module['url'].'/save'; ?>" method="post" enctype="multipart/form-data"  autocomplete="off">
  <div class="modal-header">
    <h5 class="modal-title" id="formModalTitle"><?php echo $title; ?></h5>
    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
  </div>
  <div class="modal-body">
    <div id="form-alert"><?php echo ($this->session->flashdata('message')) ? $this->session->flashdata('message') : ''; ?></div> 
    <input type="hidden" name="action" value="<?php echo $action; ?>">
 
    
    <div class='row'>
 
        <div class='col-md-12'> 
            <div class="form-group required">
                <label>Name <span class="tx-danger">*</span></label>
                <?php echo form_dropdown('users', $users, (($action == 'update') ? $data->users : null), 'class="form-control select2"'.(($action == 'update') ? 'disabled' : 'required')); ?>
            </div> 
            <div class="form-group required">
                <label>The Type of Access Rights <span class="tx-danger">*</span></label>
                <?php echo form_dropdown('group_id', $group_id, (($action == 'update') ? $data->group_id : null), 'class="form-control select2"  required'); ?>
            </div> 
        </div>
    </div>
  </div>
  <div class="modal-footer">
    <button type="submit" class="btn btn-primary waves-effect waves-light round">Save</button>
    <button type="button" class="btn btn-secondary waves-effect waves-light round" data-dismiss="modal">Cancel</button>
  </div>
</form>
  

<script type="text/javascript">
  
  $(function() {      
     
    $("#form").validate({
      errorElement: "span",
      errorClass: 'help-block',
      highlight: function (element) {
        $(element).parent().addClass('error');
      },
      unhighlight: function (element) {
        $(element).parent().removeClass('error');
      },
      submitHandler: function (form) {
        $('#loading').show();
        $(form).ajaxSubmit({
          success: function (response) {
            response = JSON.parse(response);
            if (response.status === 'success') {
              toastr.success(response.message, 'Success', {"closeButton": true});
              module.table.ajax.reload( null, false );
              $('#formModal').modal('hide');
            } else {
              toastr.error(response.message, 'Error', {"closeButton": true});
            }
            $('#loading').hide();
          },
          error: function (data) {
            $('#loading').hide();
          }
        });
      }
    });
  });

   
</script>