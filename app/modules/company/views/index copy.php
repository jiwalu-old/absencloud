<div class="content-header">
  <div class="content-header-left mb-2">
    <div class="breadcrumbs-top">
      <h2 class="content-header-title mb-0"><?php echo $module['title_pl']; ?></h2>
    </div>
  </div>
</div>
<div class="content-body">
  <div class="card">
    <div class="card-header">
      <div class="form-row w-100">
        <div class="col-auto">
          <a href="<?php echo $module['url']; ?>/create" class="btn btn-primary btn-add round">
            Add New
          </a>
        </div>
        <div class="col-auto ml-auto btn-actions">
          <input type="hidden" name="bulk_action" value="delete" id="bulk-action-select">
          <button type="button" id="bulk-action-apply" class="btn btn-icon btn-danger rounded-circle" data-toggle="tooltip" data-original-title="Delete selected items" disabled><i class="feather icon-trash-2"></i></button>
          <a href="<?php echo $module['url']; ?>/export" class="btn btn-icon btn-primary rounded-circle" data-toggle="tooltip" data-original-title="Download"><i class="feather icon-download"></i></a>
        </div>
      </div>
    </div>
    <div class="card-content">
      <div class="card-body">
        <table id="table-grid" class="table table-condensed table-striped table-hover">
          <thead>
            <tr>
              <th style="width:20px">
                <div class="vs-checkbox-con vs-checkbox-primary">
                  <input type="checkbox" name="checkAll" value="" id="checkAll">
                  <span class="vs-checkbox vs-checkbox-sm">
                    <span class="vs-checkbox--check">
                      <i class="vs-icon feather icon-check"></i>
                    </span>
                  </span>
                </div>
              </th>
              <?php
              if ($table['columns']) {
                foreach ($table['columns'] as $key => $column) {
                  echo '<th style="width:'.$column['width'].'">'.$column['title'].'</th>';
                }
              }
              ?>
            </tr>
          </thead>
          <tfoot>
            <tr>
              <th></th>
              <?php if ($table['columns']): foreach ($table['columns'] as $key => $column): ?>
              <th>
                <?php if ($column['filter']['type'] == 'text') : ?>
                <input type="text" class="form-control" placeholder="<?php echo lang('list_search'); ?>">
                <?php elseif ($column['filter']['type'] == 'date') : ?>
                <input type="text" class="form-control date" placeholder="<?php echo lang('list_search'); ?>">
                <?php elseif ($column['filter']['type'] == 'dropdown') : ?>
                  <?php $dropdown_class = (isset($column['filter']['class'])) ? 'form-control '.$column['filter']['class'] : 'form-control'; ?>
                  <?php echo form_dropdown('filter', $column['filter']['dropdown'], NULL, 'class="'.$dropdown_class.'"'); ?>
                <?php endif; ?>
              </th>
              <?php endforeach; endif; ?>
            </tr>
          </tfoot>
        </table>
      </div>
    </div>
  </div>
</div>
<?php
$columns = '{"data":"check"},';
if ($table['columns']) {
  foreach ($table['columns'] as $key => $column) {
    $columns .= '{"data":"'.$key.'", "name":"'.$column['name'].'"},';
  }
}
?>
<script type="text/javascript">
var current_url = '<?php echo current_url(); ?>';
var module = {
  url: "<?php echo $module['url']; ?>",
  hash: "<?php echo $this->security->get_csrf_hash(); ?>"
};
$(document).ready(function () {
  module.table = $('#table-grid').on('processing.dt', function (e, settings, processing) {
    if (processing === true) {
      $('.card').addClass('is-loading');
    } else {
      $('.card').removeClass('is-loading');
    }
  }).DataTable({
    "ajax": current_url + "/get_list",
    "columns": [<?php echo $columns; ?>],
    "columnDefs": [{
      orderable: false,
      targets: [<?php echo $table['disable_sorting']; ?>]
    }],
    "order": [
      [<?php echo $table['default_sort_col'].', "'.$table['default_sort_order'].'"'; ?>]
    ]
  });
});
</script>

<div class="modal fade show" id="formModal" tabindex="-1" role="dialog" aria-labelledby="formModalTitle" aria-modal="true">
  <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable modal-lg" role="document">
    <div class="modal-content">
    </div>
  </div>
</div>
<div id="loading" style="display:none">
  <div class="spinner-border text-primary" role="status">
    <span class="sr-only">Loading...</span>
  </div>
</div>