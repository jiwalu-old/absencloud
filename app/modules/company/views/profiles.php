<form id="form" action="<?php echo $module['url'].'/save'; ?>" method="post" enctype="multipart/form-data">
 
    <div class="card">
        <div class="card-header">
            <div class="form-row w-100">
                <div class="col-auto">
                <h5  ><?php echo $title; ?></h5>
                </div> 
            </div>
        </div>

        <div class="card-body">
            <div id="form-alert"><?php echo ($this->session->flashdata('message')) ? $this->session->flashdata('message') : ''; ?></div>
           
            <input type="hidden" name="id" value="<?php echo encode($data->id); ?>">
           
            <div class="row">
            <div class="col-sm-6">
                  
                  <div class="form-group required">
                    <label>Company Name</label>
                    <input class="form-control" type="text" name="name" value="<?php echo $data->name; ?>" required>
                  </div>
                  
                  <div class="form-group required">
                    <label>Business Fields</label>
                    <input class="form-control" type="text" name="business_field" value="<?php echo $data->business_field; ?>" required>
                  </div>
                  
                  <div class="form-group required">
                    <label>Province</label> 
                    <input class="form-control" type="text" name="province" value="<?php echo $data->province; ?>" required>
                  </div>
                  
                  <div class="form-group required">
                    <label>City / Regency</label>
                    <input class="form-control" type="text" name="city" value="<?php echo $data->city; ?>" required>
                  </div>
                  <div class="form-group required">
                    <label>Zip Code</label>
                    <input class="form-control" type="text" name="zip_code" value="<?php echo $data->zip_code; ?>" >
                  </div>
                   
                  <div class="form-group required">
                    <label>Address</label>
                    <textarea class="form-control" name="address" ><?php echo $data->address; ?></textarea>
                  </div>
                  
                  <div class="form-group required">
                    <label>Time Zone</label>
                    <input class="form-control" type="text" name="time_zone" value="<?php echo $data->time_zone; ?>" >
                  </div>


                   
            </div>

            <div class="col-sm-6"> 
                  <div class="form-group required">
                    <label>Phone Number</label>
                    <input class="form-control" type="text" name="phone" value="<?php echo $data->phone; ?>" required>
                  </div>

                  <div class="form-group required">
                    <label>Fax Number</label>
                    <input class="form-control" type="text" name="fax" value="<?php echo $data->fax; ?>" >
                  </div>
                  
                  <div class="form-group required">
                    <label>Email</label>
                    <input class="form-control" type="text" name="email" value="<?php echo $data->email; ?>" >
                  </div>
                  
                  <div class="form-group required">
                    <label>Website</label>
                    <input class="form-control" type="text" name="website" value="<?php echo $data->website; ?>" >
                  </div>
                <div class="form-group">
                <label>&nbsp;</label>
                <input type="file" name="image" class="form-control dropify" data-height="250" <?php echo  'data-default-file="'.$baseurl.'app-assets/images/user/'.$data->image.'"'; ?> >
                </div> 
            </div>
            </div>
        </div>
        <div class="card-footer">
            <button type="submit" class="btn btn-primary waves-effect waves-light round">Save</button> 
        </div>
    </div>

</form>

<?php $this->load->css('app-assets/vendors/dropify/dist/css/dropify.min.css'); ?>
<?php $this->load->js('app-assets/vendors/dropify/dist/js/dropify.min.js'); ?>

<script type="text/javascript"> 
  
$(function() {   

  $('.dropify').dropify({
            messages: {
                default: 'Drag and drop to select image',
                replace: 'Replace',
                remove:  'Remove',
                error:   'error'
            }
        });

  $("#form").validate({
    errorElement: "span",
    errorClass: 'help-block',
    highlight: function (element) {
      $(element).parent().addClass('error');
    },
    unhighlight: function (element) {
      $(element).parent().removeClass('error');
    },
    submitHandler: function (form) {
      $('#loading').show();
      $(form).ajaxSubmit({
        success: function (response) {
          response = JSON.parse(response);
          if (response.status === 'success') {
            toastr.success(response.message, 'Success', {"closeButton": true}); 
          } else {
            toastr.error(response.message, 'Error', {"closeButton": true});
          }
          $('#loading').hide();
        },
        error: function (data) {
          $('#loading').hide();
        }
      });
    }
  });
});
 
</script>