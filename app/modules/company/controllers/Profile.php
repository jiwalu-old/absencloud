<?php defined('BASEPATH') or exit('No direct script access allowed!');

class Profile extends App
{

  public function __construct()
  {
    parent::__construct();

    
    $this->load->library('upload');
    $this->load->model('companies'); 

    $this->data = [
      'module' => [
        'name' => 'company',
        'title' => 'company',
        'title_pl' => 'companys',
        'url' => site_url('company/profile')
      ],
      'menu' => ['menu' => 'company', 'submenu' => 'profile'],
      'error_message' => $this->session->flashdata('error_message'),
      'success_message' => $this->session->flashdata('success_message'), 
      'status' => [
        '' => '- Select -',
        'active' => 'Active',
        'inactive' => 'Inactive'
      ]
    ];
  }

  public function index()
  { 
    $this->privileged(['superadmin', 'admin']);
    if (!$this->input->is_ajax_request()) {
      $this->output->set_template('default');
      $this->output->set_title($this->data['module']['title_pl']);
    }
    $data = $this->get_data(encode(userinfo('id')));
      
    $this->data['title'] = 'Profile';
    $this->data['baseurl'] = base_url();
    $this->data['data'] = $data[0];
    $this->data['is_profile'] = true;
        // $this->data['groups'] = $this->users_group_read->as_dropdown('name')->order_by('id','desc')->get_all();

    if (!$this->input->is_ajax_request()) {
      $this->output->set_template('default');
      $this->output->set_title($this->data['title']);
    }

    $this->load->view('profiles', $this->data);
  }
 

  // public function create()
  // {
  //   $this->data['action'] = 'create';
  //   $this->data['title'] = 'Add New ' . $this->data['module']['title'];
  //   $this->data['groups'] = $this->users_group_read->as_dropdown('title')->order_by('id', 'desc')->get_all(['name !=' => 'superadmin']);

  //   if (!$this->input->is_ajax_request()) {
  //     $this->output->set_template('default');
  //     $this->output->set_title($this->data['title']);
  //   }

  //   $this->load->view('form', $this->data);
  // }

  // public function update($id)
  // {
  //   $this->data['action'] = 'update';
  //   $this->data['title'] = 'Edit ' . $this->data['module']['title'];
  //   $this->data['data'] = $this->get_data($id);
  //   $this->data['groups'] = $this->users_group_read->as_dropdown('title')->order_by('id', 'desc')->get_all(['name !=' => 'superadmin','company_id' => userinfo('company_id')]);

  //   if (!$this->input->is_ajax_request()) {
  //     $this->output->set_template('default');
  //     $this->output->set_title($this->data['title']);
  //   }

  //   $this->load->view('form', $this->data);
  // }
 

  public function save()
  {
    $this->input->is_ajax_request() or exit('No direct script access allowed!');
    $image='';
    $config['upload_path'] = './app-assets/images/user/';  
    $config['allowed_types'] = 'gif|jpg|png|jpeg|bmp';
    $config['encrypt_name'] = TRUE; 
    $this->upload->initialize($config);
    if(!empty($_FILES['image']['name']))
    {
        if ($this->upload->do_upload('image'))
          $gbr = $this->upload->data();
          $image=$gbr['file_name'];  
        
    }
    $return = [
      'status' => 'success',
      'message' => ''
    ];

    $data = $this->input->post(null, true);
     

    if($image!=''){ 
      $data['image'] = $image;
    }else{
      unset($data['image']); 
    }
     
    
    $this->form_validation->set_rules('name','Company Name','required');
    $this->form_validation->set_rules('business_field','Business Field','required');
    $this->form_validation->set_rules('province','Province','required');
    $this->form_validation->set_rules('city','City / Regency','required');
    $this->form_validation->set_rules('phone','Phone','required');

    if ($this->form_validation->run() === true) {

        
        $id = decode($data['id']);
        unset($data['id']);
        $save = $this->companies->update($data, ['id' => $id]);
        if ($save) {
          $return['status'] = 'success';
          $return['message'] = lang('update_success_message');
        } else {
          $return['status'] = 'error';
          $return['message'] = lang('update_error');
        }
     
    } else {
      $return['status'] = 'error';
      $return['message'] = validation_errors();
    }

    echo json_encode($return);
  }
 
  public function delete($id = null)
  {
    $delete = false;
    if (!is_null($id)) {
      $id = decode($id);
      $delete = $this->companies->delete($id);
    } else {
      $data = $this->input->post('data', true);
      if ($data) {
        foreach ($data as $id) {
          $id = decode($id);
          $delete = ($id != 1) ? $this->companies->delete($id) : false;
        }
      }
    }

    if ($delete) {
      echo 'success';
      $this->session->set_flashdata('success_message', successMessage(lang('delete_success_message')));
    } else {
      echo 'error';
      $this->session->set_flashdata('error_message', errorMessage(lang('delete_error_message')));
    }
  }

  public function get_data($id)
  {
    // echo $id; exit();
    $id = decode($id);
    $data = $this->companies->get_data($id);
    // echo json_encode($data); exit();
    if ($data) {
      return $data;
    } else {
      $this->session->set_flashdata('error_message', errorMessage(lang('not_found')));
      redirect($this->data['module']['url']);
    }
  }
 
}