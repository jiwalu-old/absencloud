<?php defined('BASEPATH') or exit('No direct script access allowed');

class Companies extends MY_Model
{
    public $table = 'companies';
    public $primary_key = 'id';

    public function __construct()
    {
        parent::__construct();
        $this->timestamps = FALSE;
    }

    public function getPartner(){ 
        $this->db->select("c.id,c.name");
        $this->db->from("$this->table c");
        $this->db->join("app.partners p", "p.partner=c.id", "inner"); 
        $this->db->where("p.company",userinfo('company_id')); 
        $result = $this->db->get();
        return ($result)?$result->result_array():false;
    }
    public function get_data($id){ 
        $this->db->select("c.*");
        $this->db->from("$this->table c");
        $this->db->join("app.users u", "u.company_id=c.id", "inner"); 
        $this->db->where("u.id",$id); 
        $result = $this->db->get();
        return ($result)?$result->result():false;
    }
}
