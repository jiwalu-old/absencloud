<?php defined('BASEPATH') or exit('No direct script access allowed');

class User_groups extends MY_Model
{
    public $table = 'user_groups';
    public $primary_key = 'id';

    public function __construct()
    {
        parent::__construct();
        $this->timestamps = FALSE;
    }

    public function get_vehicle($id){ 
        $result = $this->db->query("SELECT (privileges::json->>'vehicle')as vehicles from app.user_groups where id='$id'")->row('vehicles');
        return ($result)?$result:false;
    }
}
