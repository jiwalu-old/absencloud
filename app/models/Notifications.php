<?php defined('BASEPATH') or exit('No direct script access allowed');

class Notifications extends MY_Model
{ 
    public $table = 'notifications';
    public $primary_key = 'id';

    public function __construct()
    {
        parent::__construct();
        $this->timestamps = false;
    }

    function getListData($offset){
        $id = userinfo('company_id');
        $process = $this->db->query("SELECT id,title,url,is_read,created_at from notifications where company_id='$id' ORDER BY id desc limit 10 offset $offset");
        return $process->result();
    }
 
}
