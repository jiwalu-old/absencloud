<?php defined('BASEPATH') or exit('No direct script access allowed');

class Users extends MY_Model
{
    public $table = 'users';
    public $primary_key = 'id';

    public function __construct()
    {
        parent::__construct();
        $this->soft_deletes = TRUE;
    }

    public function get_list()
    {
        $this->load->library('datatable'); 
        $this->datatable->select("id,name,email,status,last_login");
        $this->datatable->from($this->table);  
        $this->datatable->where("group_id is not null"); 

        return json_decode($this->datatable->generate(), true);
    }

    public function get_data($id){
        $this->select('id as users, group_id');
        $this->from($this->table);
        $this->where('id',$id);
        $query = $this->db->get();
        return $query->result();
    }
}
