<?php (defined('BASEPATH')) OR exit('No direct script access allowed');

/**
 * Jiwalu Framework
 * A framework for PHP development
 *
 * @package     Jiwalu
 * @author      Jiwalu Studio
 * @copyright   Copyright (c) 2019, Jiwalu Studio (https://www.jiwalu.id)
 */

// if (!function_exists('get_address')) {
//     function get_address($latitude, $longitude) {
//         $address = 'Unknown address';
//         // $get_address_content = file_get_contents('https://nominatim.openstreetmap.org/reverse?email=sandi4code@gmail.com&zoom=18&format=json&lat='.$latitude.'&lon='.$longitude);
//         // if ($get_address_content) {
//         //     $address_content = json_decode($get_address_content);
//         //     if (isset($address_content->display_name)) {
//         //         $address = $address_content->display_name;
//         //     }
//         // }

//         return $address;
//     }
// }

if (!function_exists('get_address')) {
    function get_address($latitude, $longitude) {
        $address = NULL;
        $get_address_content = file_get_contents('https://us1.locationiq.com/v1/reverse.php?key=843169e3da0e95&format=json&lat='.$latitude.'&lon='.$longitude);
        if ($get_address_content) {
            $address_content = json_decode($get_address_content);
            if (isset($address_content->display_name)) {
                $address = $address_content->display_name;
            }
        }

        return $address;
    }
}

// if (!function_exists('get_address')) {
//     function get_address($latitude, $longitude) {
        // $address = 'Unknown address';
        // $get_address_content = file_get_contents('https://maps.googleapis.com/maps/api/geocode/json?latlng='.$latitude.','.$longitude.'&key='.MAPS_APIKEY);
        // if ($get_address_content) {
        //     $address_content = json_decode($get_address_content);
        //     if ($address_content->status == "OK") {
        //         $address = $address_content->results[0]->formatted_address;
        //     }
        // }

        // return $address;
//     }
// }

/* End of file maps_helper.php */
/* Location: ./helpers/maps_helper.php */
