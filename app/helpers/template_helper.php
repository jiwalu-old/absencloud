<?php (defined('BASEPATH')) OR exit('No direct script access allowed');

/**
 * Jiwalu Framework
 * A framework for PHP development
 *
 * @package     Jiwalu Framework
 * @author      Jiwalu Studio
 * @copyright   Copyright (c) 2016, Jiwalu Studio (https://www.jiwalu.id)
 */

if (!function_exists('excerpt')) {
    function excerpt($content, $length = 200, $striptags = FALSE, $delimiter = ''){
        $excerpt = explode('<div style="page-break-after: always;"><span style="display:none">&nbsp;</span></div>', $content, 2);
        if(count($excerpt) > 1){
            $excerpt_fix = ($striptags!=FALSE) ? htmlspecialchars_decode(strip_tags($excerpt[0])) : closetags(htmlspecialchars_decode($excerpt[0]));
        } else {
            $excerpt_fix = ($striptags!=FALSE) ? substr(htmlspecialchars_decode(strip_tags($content)), 0, $length) : closetags(substr(htmlspecialchars_decode($content), 0, $length));
        }
        return ($length < strlen($content)) ? $excerpt_fix.$delimiter : $excerpt_fix;
    }
}

if (!function_exists('closetags')) {
    function closetags($html){
        preg_match_all('#<([a-z]+)(?: .*)?(?<![/|/ ])>#iU', $html, $result);
        $openedtags = $result[1];
        preg_match_all('#</([a-z]+)>#iU', $html, $result);
        $closedtags = $result[1];

        $len_opened = count($openedtags);

        if (count($closedtags) == $len_opened) {
            return $html;
        }

        $openedtags = array_reverse($openedtags);

        for ($i = 0; $i < $len_opened; $i++) {
            if (!in_array($openedtags[$i], $closedtags)) {
                $html .= '</' . $openedtags[$i] . '>';
            } else {
                unset($closedtags[array_search($openedtags[$i], $closedtags)]);
            }
        }
        return $html;
    }
}

if (!function_exists('is_home')) {
    function is_home()
    {
        return (site_url() == current_url()) ? true : false;
    }
}

if (!function_exists('list_pages')) {
    function list_pages($parent = 0, $child_order = 1, $depth = NULL)
    {
        $app =& get_instance();
        $output = '';
        $pages = $app->post_model->fields('id,title,slug,parent')->order_by('title')->get_all(['parent' => $parent, 'type' => 'page', 'status' => 'publish']);
        if ($pages) {
            if ($child_order > 1) {
                $output .= '<ul class="children">';
            } else {
                $output .= '<ul>';
            }

            if (is_null($depth) || $depth > $child_order) {
                $child_order++;
            }
            foreach ($pages as $page) {
                $children = '';
                $has_children_class = '';
                if (is_null($depth) || $depth > $child_order) {
                    $check_children = $app->post_model->count_rows(['parent' => $page->id, 'type' => 'page', 'status' => 'publish']);
                    if ($check_children > 0) {
                        $children = list_pages($page->id, $child_order);
                        $has_children_class = ' page_item_has_children';
                    }
                }
                $output .= '<li class="page_item'.$has_children_class.'"><a href="'.permalink_page($page).'">'.$page->title.'</a>';
                $output .= $children;
                $output .= '</li>';
                // echo '<li><a href="'.permalink_page($page).'">'.$page->title.'</a></li>';
            }
            $output .= '</ul>';
        }
        return $output;
    }
}

if (!function_exists('breadcrumb')) {
    function breadcrumb($post)
    {
        $app =& get_instance();
        $breadcrumb = '<ul>';
        if (is_object($post)) {
            $category = $app->term_model->get_category($post->id);
            if ($category) {
                if ($category->parent != 0) {
                    $parent_category = $app->term_model->get($category->parent);
                    $breadcrumb .= '.<li><a href="'.site_url($parent_category->slug).'">'.$parent_category->name.'</a></li>';
                    $breadcrumb .= '.<li><a href="'.site_url($parent_category->slug.'/'.$category->slug).'">'.$category->name.'</a></li>';
                    $breadcrumb .= '.<li><a href="'.site_url($parent_category->slug.'/'.$category->slug.'/'.$post->slug).'">'.$post->title.'</a></li>';
                } else {
                    $breadcrumb .= '.<li><a href="'.site_url($category->slug).'">'.$category->name.'</a></li>';
                    $breadcrumb .= '.<li><a href="'.site_url($category->slug.'/'.$post->slug).'">'.$post->title.'</a></li>';
                }
            } else {
                $breadcrumb .= '<li><a href="'.site_url($post->slug).'">'.$post->title.'</a></li>';
            }
        } else {
            $breadcrumb .= '<li><a href="'.site_url().'">Home</a></li>';
        }

        return $breadcrumb;
    }
}

if (!function_exists('permalink')) {
    function permalink($post)
    {
        $app =& get_instance();
        $app->load->model('categories');

        if (is_object($post)) {
            $category = $app->categories->find($post->category_id);
            if ($category) {
                if ($category->parent_id != 0) {
                    $parent_category = $app->categories->find($category->parent_id);
                    $permalink = site_url($parent_category->slug.'/'.$category->slug.'/'.$post->slug);
                } else {
                    $permalink = site_url($category->slug.'/'.$post->slug);
                }
            } else {
                $permalink = site_url($post->slug);
            }
        } else {
            $permalink = site_url();
        }

        return $permalink;
    }
}
if (!function_exists('permalink_community')) {
    function permalink_community($discussion)
    {
        $app =& get_instance();
        $app->load->model('community/topics');

        if (is_object($discussion)) {
            $topic = $app->topics->find($discussion->topic_id);
            if ($topic) {
                $permalink = site_url('community/'.$topic->slug.'/'.$discussion->slug);
            } else {
                $permalink = site_url('community/'.$discussion->slug);
            }
        } else {
            $permalink = site_url('community');
        }

        return $permalink;
    }
}

if (!function_exists('permalink_page')) {
    function permalink_page($post)
    {
        return site_url(get_permalink_page_recursive($post));
    }
}

if (!function_exists('get_permalink_page_recursive')) {
    function get_permalink_page_recursive($post)
    {
        $app =& get_instance();
        $permalink = '';
        if (is_object($post)) {
            if ($post->parent == 0) {
                $permalink .= $post->slug;
            } else {
                $parent = $app->post_model->get($post->parent);
                if ($parent) {
                    $permalink .= get_permalink_page_recursive($parent);
                    $permalink .= '/'.$post->slug;
                } else {
                    $permalink .= $post->slug;
                }
            }
        }
        return $permalink;
    }
}

if (!function_exists('permalink_category')) {
    function permalink_category($category, $additional_slug = '')
    {
        $app =& get_instance();
        if (is_object($category)) {
            if ($category->parent != 0) {
                $parent_category = $app->term_model->get($category->parent);
                $permalink = site_url($additional_slug.$parent_category->slug.'/'.$category->slug);
            } else {
                $permalink = site_url($additional_slug.$category->slug);
            }
        } else {
            $permalink = '';
        }

        return $permalink;
    }
}

if(!function_exists('read_more')) {
    function read_more($text, $limit = 50)
    {
        $text = preg_replace('/(?:<|&lt;).+?(?:>|&gt;)/', '', $text);
        if (str_word_count($text, 0) > $limit) {
          $words = str_word_count($text, 2);
          $pos = array_keys($words);
          $text = substr($text, 0, $pos[$limit]);
        }
        return $text.'...';
    }
}

if (!function_exists('get_option')) {
    function get_option($name)
    {
        $app =& get_instance();
        $setting = $app->option_model->fields('id,value')->get(['name' => $name]);
        return ($setting) ? $setting->value : '';
    }
}

if (!function_exists('get_assets_url')) {
    function get_assets_url()
    {
        $theme = get_option('theme');
        return base_url().'themes/'.$theme.'/assets';
    }
}

if (!function_exists('get_theme_url')) {
    function get_theme_url()
    {
        $theme = get_option('theme');
        return base_url().'themes/'.$theme;
    }
}

if (!function_exists('get_sidebar')) {
    function get_sidebar()
    {
        $app =& get_instance();
        return $app->load->view($app->data['theme'].'/sidebar');
    }
}

if (!function_exists('get_footer')) {
    function get_footer()
    {
        $app =& get_instance();
        return $app->load->view($app->data['theme'].'/footer');
    }
}

if (!function_exists('get_header')) {
    function get_header()
    {
        $app =& get_instance();
        return $app->load->view($app->data['theme'].'/header');
    }
}

if (!function_exists('get_postmeta')) {
    function get_postmeta($post_id, $name)
    {
        $app =& get_instance();
        $meta = $app->postmeta_model->fields('value')->get(['post_id' => $post_id,'name' => $name]);
        return ($meta) ? $meta->value : '';
    }
}

if (!function_exists('get_author')) {
    function get_author($post)
    {
        $app =& get_instance();
        $user = $app->user_model->get($post->created_by);
        return ($user) ? $user : '';
    }
}

if (!function_exists('get_avatar')) {
    function get_avatar($user, $size = 150)
    {
        if (!empty($user->image)) {
            $image = thumb($user->image, $size, $size);
        } else {
            $image = 'http://0.gravatar.com/avatar/default?s='.$size.'&d=mm&r=g';
        }
        return '<img src="'.$image.'" width="'.$size.'" height="'.$size.'" alt="'.$user->name.'" class="avatar photo avatar-default">';
    }
}

if (!function_exists('video_thumb')) {
    function video_thumb($video_id)
    {
        return 'https://img.youtube.com/vi/'.$video_id.'/mqdefault.jpg';
    }
}

if (!function_exists('icon_url')) {
    function icon_url($image_url)
    {
        if (!empty($image_url)) {
            $url_parts = pathinfo($image_url);
            // var_dump($url_parts);
            $icon_url = $url_parts['dirname'].'/'.$url_parts['filename'].'-icons.'.$url_parts['extension'];
        } else {
            $icon_url = base_url('themes/default/assets/img/activity-thumb2.jpg');
        }

        return $icon_url;
    }
}

if (!function_exists('thumb_url')) {
    function thumb_url($image_url)
    {
        if (!empty($image_url)) {
            $url_parts = pathinfo($image_url);
            // var_dump($url_parts);
            $thumb_url = $url_parts['dirname'].'/'.$url_parts['filename'].'-thumbs.'.$url_parts['extension'];
        } else {
            $thumb_url = base_url('themes/default/assets/img/activity-thumb2.jpg');
        }

        return $thumb_url;
    }
}

if (!function_exists('thumb')) {
    function thumb($fullname, $width, $height)
    {
        $app = &get_instance();
        if (!empty($fullname)) {
            $parsed_url = parse_url($fullname, PHP_URL_PATH);
            $dir = pathinfo($parsed_url, PATHINFO_DIRNAME);
            $extension = pathinfo($fullname, PATHINFO_EXTENSION);
            $filename = pathinfo($fullname, PATHINFO_FILENAME);
            $image_path = "." . $dir . "/" . $filename . "." . $extension;
            $image_thumb = "." . $dir . "/" . $filename . "-" . $width . 'x' . $height . "." . $extension;
            $filename_original = $filename . "." . $extension;
            $filename_returned = $filename . "-" . $width . 'x' . $height . "." . $extension;
            $image_returned = str_replace($filename_original, $filename_returned, $fullname);

            if (file_exists($image_path)) {
                if (!file_exists($image_thumb)) {
                    $app->load->library('image_moo');
                    $app->image_moo
                        ->load($image_path)
                        ->resize_crop($width, $height)
                        ->save($image_thumb);
                }
                return $image_returned;
            } else {
                return $image_returned;
                // return $fullname;
            }
        }
    }
}

if (!function_exists('get_timeago')) {
    function get_timeago( $date )
    {
        $estimate_time = time() - strtotime($date);

        if( $estimate_time < 1 )
        {
            return 'beberapa saat lalu';
        }

        $condition = array(
            12 * 30 * 24 * 60 * 60  =>  'tahun',
            30 * 24 * 60 * 60       =>  'bulan',
            24 * 60 * 60            =>  'hari',
            60 * 60                 =>  'jam',
            60                      =>  'menit',
            1                       =>  'detik'
        );

        foreach( $condition as $secs => $str )
        {
            $d = $estimate_time / $secs;

            if( $d >= 1 )
            {
                $r = round( $d );
                return $r . ' ' . $str . ' lalu';
            }
        }
    }
}
/* End of file template_helper.php */
/* Location: ./system/helpers/template_helper.php */
